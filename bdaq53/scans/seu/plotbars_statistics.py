import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime

YLIM_LOW_BARS = 0
YLIM_HIGH_BARS = 100


# Plotting:
# This method adds the value of each bar on top of it: element is each bar of a system:
def autolabel(ax, system):
    for element in system:
        height = element.get_height()
        if (height < YLIM_HIGH_BARS - 0.08 * YLIM_HIGH_BARS):
            ax.annotate('{}%'.format(height), fontsize=15, rotation=90, xy=(element.get_x() + element.get_width() / 2, height), xytext=(0, 3), textcoords="offset points", ha='center', va='bottom')


def position_bars(len_data, width):
    if (len_data % 2) == 0:
        factors = list(range(-int(len_data / 2), int(len_data / 2)))
        relative_positions = [width * (x + 0.5) for x in factors]
    else:
        factors = list(range(-int(len_data / 2), int(len_data / 2) + 1))
        relative_positions = [width * x for x in factors]
    return relative_positions


def one_bar_light(ax, user_data, position, width, hatch_value):
    # Default settings:
    data = {'y': None, 'label': None, 'edge_color': 'b', 'color': 'b', 'linewidth': 0}

    # Updating settings according to user:
    for key, value in user_data.items():
        data[key] = value
    bar_output = ax.bar(position, data['y'], width, label=data['label'], color=data['color'], edgecolor=data['edge_color'], linewidth=data['line_width'], hatch=hatch_value)
    autolabel(ax, bar_output)


def plot_bars(data_set, user_global_settings={}):
    date_time = datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss")
    global_settings = {'width': 0.35, 'figure_size': [10, 5], 'title': 'Title text', 'title_fontsize': 25, 'title_fontweight': 'bold', 'x_label_fontsize': 20, 'x_label_rotation': 0, 'y_label': 'ylabel text (units)', 'y_label_fontsize': 20, 'y_label_rotation': 90, 'x_ticks': None, 'y_ticks': None, 'x_ticks_fontsize': 15, 'y_ticks_fontsize': 15, 'rotation_xticks': 0, 'rotation_yticks': 90, 'text': '', 'text_xcoordinate': 0, 'text_ycoordinate': 0, 'text_color': "blue", 'text_font_size': 10, 'text_rotation': 270, 'legend_inside': 'None', 'legend_location': 'best', 'legend_fontsize': 15, 'legend_point_size': 70, 'output_name': date_time + '_plot', 'output_extension': 'jpg', 'grid': True, 'grid_color': 'gainsboro', 'grid_linestyle': '-', 'grid_linewidth': 2, 'output_resolution_dpi': 400, 'labels': None}

    # Updating global settings according to user:
    for key, value in user_global_settings.items():
        global_settings[key] = value

    # GLOBAL SETTINGS:
    fig, ax = plt.subplots(figsize=(global_settings['figure_size'][0], global_settings['figure_size'][1]))
    ax.set_title(global_settings["title"], fontsize=global_settings["title_fontsize"], fontweight=global_settings["title_fontweight"])
    ax.set_ylim(global_settings["ylim"])
    ax.set_ylabel(global_settings["y_label"], fontsize=global_settings["y_label_fontsize"], rotation=global_settings["y_label_rotation"])
    label_locations = np.arange(len(global_settings["labels"]))  # the label locations
    ax.set_xticks(label_locations)
    ax.set_xticklabels(global_settings["labels"], fontsize=global_settings["x_label_fontsize"])

    # Plotting all inputs:

    width = global_settings["width"]  # the width of the bars

    if type(data_set) == dict:  # this means there's only one system
        ll = []
        ll.append(data_set)
        data_set = ll
    for item, var in enumerate(data_set):
        len_data = len(data_set)
        relative_positions = position_bars(len_data, width)
        position = label_locations + relative_positions[item]
        one_bar_light(ax, var, position, width, global_settings["hatches"][item])

    # EXTRAS: LEGENDS, TICKS, TEXT BOXES, ARROWS, GRID
    # TODO chartBox is used to put the legend outside, review how to do this properly
    if(global_settings['legend_inside']):
        ax.legend(loc=global_settings["legend_location"], fontsize=global_settings["legend_fontsize"])

    else:
        box = ax.get_position()  # getting position of the axes to reduce the plot and place legend with respect to them
        ax.set_position([box.x0, box.y0, box.width, box.height])  # Reducing axes to 90% to place legend outside:
        ax.legend(loc=global_settings["legend_location"], fontsize=global_settings["legend_fontsize"], bbox_to_anchor=(1, 1))
        print('legend outside')
    if(global_settings['legend_inside'] == 'None'):
        print('No legend generated')
        ax.legend().set_visible(False)

    if global_settings["grid"]:
        ax.grid(True)  # TODO fix this, color = global_settings['grid_color'], linestyle = global_settings['grid_linestyle'], linewidth = global_settings['grid_linewidth'])

    plt.tight_layout()
    # SAVING THE PLOT:
    fig.savefig(global_settings["output_name"] + '.' + global_settings["output_extension"], dpi=global_settings["output_resolution_dpi"])  # TODO also extension, bng, pdf...
    plt.close(fig)


##########################################################################################################
##########################################################################################################
