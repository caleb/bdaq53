from datetime import datetime
import os
import time
import matplotlib.pyplot as plt

# Parameters for temperature file
SEPARATOR = ';'
HEADER_TEMP_FILE = "DATE;LINUX_TIME;t(s);CAPTOR_NAME;TEMP"
COLORS = {'TEMP1': '#800000', 'TEMP2': '#f58231', 'TEMP3': '#004e80', 'TEMP4': '#3cb44b', 'TEMP5': '#000075'}
OUTPUT_FOLDER = '00_SEU_TEMP_DATA/'  # The root folder where all power data are
DATA_FOLDER = OUTPUT_FOLDER + datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss") + '_TEMP_DATA'  # the specific folder where the data of this test are
OUTPUT_FILE = datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss") + '_temp_data'


# Util to make folders:
def make_folders(folders):
    for element in folders:
        if not os.path.exists(element):
            os.makedirs(element)


# Util to write text file:
def write_textfile(directory, text=None, filename=datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss") + '_SEU', header='None'):
    if not os.path.exists(directory + '/' + filename + ".csv"):
        out_text = open(directory + '/' + filename + ".csv", "a")
        tobewritten = "%" + header + "\n"
        out_text.write(tobewritten)
        out_text.close()
    if text is not None:
        out_text = open(directory + '/' + filename + ".csv", "a")
        out_text.write(text)
        out_text.close()
    return(filename)


def _temperature(itkpixv1, t1, iteration):
    folders = [OUTPUT_FOLDER, DATA_FOLDER]
    make_folders(folders)

    mean_temp, temp = itkpixv1.get_temperature_sensors(1, 15)
    for key, value in temp.items():
        for types, tempe in value.items():
            if types == 'temp':
                power_log = datetime.now().strftime("%Y_%m_%d_%Hh%Mm%Ss") + SEPARATOR + str(time.time()) + SEPARATOR + str(time.time() - t1) + SEPARATOR + str(key) + SEPARATOR + str(tempe) + '\n'
                write_textfile(DATA_FOLDER, power_log, OUTPUT_FILE, HEADER_TEMP_FILE)
    plot_voltage_current(iteration)


def import_data():

    captor_T = []
    captor_B = []
    captor_A = []
    captor_D = []
    captor_C = []
    time_T = []
    time_B = []
    time_A = []
    time_D = []
    time_C = []

    with open(DATA_FOLDER + '/' + OUTPUT_FILE + '.csv') as f:
        content = f.readlines()
    f.close()

    for index, element in enumerate(content):
        if index > 0:

            if element.split(';')[3] == 'TEMPSENS_T':
                captor_T.append(float(element.split(';')[4]))
                time_T.append(float(element.split(';')[2]))
            elif element.split(';')[3] == 'TEMPSENS_B':
                captor_B.append(float(element.split(';')[4]))
                time_B.append(float(element.split(';')[2]))
            elif element.split(';')[3] == 'TEMPSENS_A':
                captor_A.append(float(element.split(';')[4]))
                time_A.append(float(element.split(';')[2]))
            elif element.split(';')[3] == 'TEMPSENS_D':
                captor_D.append(float(element.split(';')[4]))
                time_D.append(float(element.split(';')[2]))
            else:
                captor_C.append(float(element.split(';')[4]))
                time_C.append(float(element.split(';')[2]))
    return(time_T, time_B, time_A, time_D, time_C, captor_T, captor_B, captor_A, captor_D, captor_C)


def plot_voltage_current(iteration):
    time_T, time_B, time_A, time_D, time_C, captor_T, captor_B, captor_A, captor_D, captor_C = import_data()

    plt.figure(1)
    plt.subplot(111)
    plt.plot(time_T, captor_T, color='green', marker='o', label='Captor T')
    plt.title('Temperature')
    plt.ylabel('T(°c)')
    plt.xlabel('t(s)')

    plt.subplot(111)
    plt.plot(time_B, captor_B, color='blue', marker='o', label='Captor B')
    plt.ylabel('T(°c)')
    plt.xlabel('t(s)')

    if iteration == 1:  # put one time the legend
        plt.legend()
    plt.savefig(DATA_FOLDER + '/' + OUTPUT_FILE + '_temp1' + '.png')

    plt.figure(2)
    plt.subplot(111)
    plt.plot(time_A, captor_A, color='red', marker='o', label='Captor A')
    plt.title('Temperature')
    plt.ylabel('T(°c)')
    plt.xlabel('t(s)')

    plt.subplot(111)
    plt.plot(time_D, captor_D, color='orange', marker='o', label='Captor D')
    plt.ylabel('T(°c)')
    plt.xlabel('t(s)')

    plt.subplot(111)
    plt.plot(time_C, captor_C, color='green', marker='o', label='Captor C')
    plt.ylabel('T(°c)')
    plt.xlabel('t(s)')

    if iteration == 1:  # put one time the legend
        plt.legend()
    plt.savefig(DATA_FOLDER + '/' + OUTPUT_FILE + '_temp2' + '.png')
