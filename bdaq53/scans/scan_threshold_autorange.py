#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script scans over different amounts of injected charge
    to find the effective threshold of the enabled pixels.
    To speed up the total scan time, it starts with a coarse scan
    to find the optimal charge to start a finer scan. This fine scan
    is aborted once a defined amount of S-curves saturates.

    This enables do define a broad total range to find the threshold
    and still yield a well resolved S-curve in a short amount of time.
'''

from tqdm import tqdm
import numpy as np

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import online as oa

scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    # Start threshold scan at injection setting where a fraction of min_response of the selected
    # pixels see the fraction of hits min_hits
    'min_response': 0.01,
    'min_hits': 0.2,
    # Stop threshold scan at injection setting where a fraction of max_response of the selected
    # pixels see the fraction of hits max_hits
    'max_response': 0.99,
    'max_hits': 1.,

    'n_injections': 200,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 540,  # start value of injection
    'VCAL_HIGH_stop': 1500,  # maximum injection, can be None
    'VCAL_HIGH_step_fine': 8,  # step size during threshold scan
    'VCAL_HIGH_step_coarse': 100  # step when seaching start
}


class AutorangeThresholdScan(ScanBase):
    scan_id = 'autorange_threshold_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

        self.data.hist_occ = oa.OccupancyHistogramming(chip_type=self.chip.chip_type.lower(), rx_id=int(self.chip.receiver[-1]))

    def _scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192,
              n_injections=200, VCAL_MED=500, VCAL_HIGH_start=540, VCAL_HIGH_stop=1500, VCAL_HIGH_step_fine=8, VCAL_HIGH_step_coarse=100,
              min_response=0.01, max_response=0.99, min_hits=0.2, max_hits=1., **_):
        '''
        Threshold scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''

        self.data.start_data_taking = False     # Indicates if threshold scan has already started
        scan_param_id = -1                      # Do not store data before S-curve sampling
        vcal_high = VCAL_HIGH_start             # Start value

        # Calculated max pixels that can respond
        sel_pix = self.chip.masks['enable'][start_column:stop_column, start_row:stop_row]
        n_sel_pix = np.count_nonzero(sel_pix)

        pbar = None

        self.log.info('Search for maxium response...')
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH_stop, vcal_med=VCAL_MED)
        with self.readout(scan_param_id=scan_param_id, callback=self.analyze_data_online):
            shift_and_inject(scan=self, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id, cache=True)

        occupancy = self.data.hist_occ.get()    # Interpret raw data and create occupancy histogram
        max_n_pix = np.count_nonzero(occupancy >= n_injections * max_hits)

        self.log.info('Maximum reponse is {0} of {1} pixels ({2:1.2f}%)'.format(max_n_pix, n_sel_pix, float(max_n_pix) / n_sel_pix * 100.))

        self.log.info('Search for best start parameter...')

        while vcal_high < VCAL_HIGH_stop:
            if not self.data.start_data_taking:
                self.log.info('Try Delta VCAL = {0}...'.format(vcal_high - VCAL_MED))

            self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
            with self.readout(scan_param_id=scan_param_id, callback=self.analyze_data_online):
                shift_and_inject(scan=self, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id, cache=True)
                if self.data.start_data_taking:
                    self.store_scan_par_values(scan_param_id=scan_param_id, vcal_high=vcal_high, vcal_med=VCAL_MED)

            occupancy = self.data.hist_occ.get()    # Interpret raw data and create occupancy histogram
            if not self.data.start_data_taking:     # Log responding pixels
                n_pix = np.count_nonzero(occupancy >= n_injections * min_hits)
                self.log.info('{0} of {1} pixels ({2:1.2f}%) see {3}% of injections'.format(n_pix, n_sel_pix, float(n_pix) / n_sel_pix * 100., int(min_hits * 100.)))
            else:
                n_pix = np.count_nonzero(occupancy >= n_injections * max_hits)
                self.log.debug('{0} of {1} pixels ({2:1.2f}%) see {3}% of injections'.format(n_pix, n_sel_pix, float(n_pix) / n_sel_pix * 100., int(max_hits * 100.)))

            if self.data.start_data_taking and n_pix >= max_n_pix:
                pbar.update(n_sel_pix - pbar.n)
                pbar.close()
                self.log.info('S-curve fully sampled. Stop scan.')
                break

            if pbar is not None:
                try:
                    pbar.update(n_pix - pbar.n)
                except ValueError:
                    pass

            if np.count_nonzero(occupancy >= n_injections * max_hits) >= n_sel_pix * max_response:  # Check for stop condition: number of pixels that see all hits
                if not self.data.start_data_taking:     # Overlooked the start condition
                    self.log.info('Start sampling S-curves for {0} enabled pixels...'.format(n_sel_pix))
                    self.data.start_data_taking = True
                    pbar = tqdm(total=n_sel_pix, unit=' Pixels')
                    scan_param_id += 1
                    # Go back two steps
                    if vcal_high - 2 * VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine > VCAL_HIGH_start:
                        vcal_high = vcal_high - 2 * VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine
                    else:
                        vcal_high = VCAL_HIGH_start
                    continue
                else:
                    pbar.update(n_sel_pix - pbar.n)
                    pbar.close()
                    self.log.info('S-curve fully sampled. Stop scan.')
                    break

            if not self.data.start_data_taking:
                # Check start condition: number of pixels with hits
                if np.count_nonzero(occupancy >= n_injections * min_hits) >= float(n_sel_pix * min_response):
                    self.log.info('Start sampling S-curves for {0} enabled pixels...'.format(n_sel_pix))
                    self.data.start_data_taking = True
                    pbar = tqdm(total=n_sel_pix, unit=' Pixels')
                    scan_param_id += 1
                    # Go back one step
                    if vcal_high - VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine > VCAL_HIGH_start:
                        vcal_high = vcal_high - VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine
                    else:
                        vcal_high = VCAL_HIGH_start
                else:
                    vcal_high += VCAL_HIGH_step_coarse
            else:  # already scanning, just increase scan pars and value
                scan_param_id += 1
                vcal_high += VCAL_HIGH_step_fine
        # Maximum charge reached and no abort triggered (break of while loop)
        else:
            self.log.warning('Maximum injection reached. Abort.')

        self.data.hist_occ.close()  # stop analysis process
        self.log.success('Scan finished')

    # Used to overwrite data storing function: self.readout.handle_data
    def analyze_data_online(self, data_tuple, receiver=None):
        raw_data = data_tuple[0]
        if self.chip.chip_type.lower() == 'itkpixv1':  # ITkPixv1 needs ptot table for analysis
            self.data.hist_occ.add(raw_data, self.ptot_table[:])
        else:
            self.data.hist_occ.add(raw_data)
        # Only store s-curve data when scan started
        if self.data.start_data_taking:
            super(AutorangeThresholdScan, self).handle_data(data_tuple, receiver)

    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            mean_thr = np.median(a.threshold_map[np.nonzero(a.threshold_map)])
            mean_noise = np.median(a.noise_map[np.nonzero(a.noise_map)])
            if np.isfinite(mean_thr):
                self.log.success('Mean threshold is {0} [Delta VCAL]'.format(int(mean_thr)))
            else:
                self.log.error('Mean threshold could not be determined!')

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

        return mean_thr, mean_noise


if __name__ == '__main__':
    with AutorangeThresholdScan(scan_config=scan_configuration) as scan:
        scan.start()
