#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test writes data into all pixel registers,
    then reads back the data and compares read and written data.
'''

import tables as tb
import numpy as np
from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import rd53a_analysis as ana
from bdaq53.analysis import rd53b_analysis as anb
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import plotting

scan_configuration = {
    'test_pattern': 0b10101010
}


class PixelRegisterScan(ScanBase):
    scan_id = 'pixel_register_scan'

    def _configure(self, TDAC=None, **_):
        pass

    def write_pixels(self, pixel_register, outer_loop=0):
        self.log.info('Starting Scan with pattern: ' + bin(pixel_register[0][0]))
        self.chip.masks['enable'] = (pixel_register[:, :]) & 0b00000001  # 7
        self.chip.masks['injection'] = (pixel_register[:, :] & 0b00000010) >> 1  # 6
        self.chip.masks['hitbus'] = (pixel_register[:, :] & 0b00000100) >> 2
        self.chip.masks['tdac'] = (pixel_register[:, :] & 0b11111000) >> 3
        self.chip.masks['lin_gain_sel'] = (pixel_register[:, :] & 0b10000000) >> 7
        if self.chip.chip_type.lower() == 'rd53a':  # special sync fe treatment
            self.chip.masks['lin_gain_sel'][128:264, :] = (pixel_register[128:164, :] & 0b10000000) >> 7
        self.chip.masks.update(force=True)
        self.h5_file.create_carray(self.h5_file.root, name='pix_data_' + str(outer_loop), title='Pixel Data',
                                   obj=pixel_register,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))

        self.chip.registers['PIX_MODE'].set(0)
        self.chip.registers['PIX_MODE'].write(0)
        self.chip.enable_monitor_data()
        self.chip.send_global_pulse('reset_monitor_data', pulse_width=4)

    def read_pixels(self, outer_loop=0):
        cols = self.chip.masks.dimensions[0]  # 400
        rows = self.chip.masks.dimensions[1]  # 384 192
        sync_multiplier = 1  # itkpixv1 can run faster than rd53a therefore rd53a --> 4
        if self.chip.chip_type.lower() == 'rd53a':
            sync_multiplier = 4

        pbar = tqdm(total=(200 * rows), unit=' Pixel steps')
        indata = self.chip.write_sync_01(write=False)
        with self.readout(scan_param_id=outer_loop):
            for col in range(0, cols, 2):
                # with self.readout(scan_param_id = col + (outer_loop * cols), timeout=20):
                (core_col, core_row, region, pixel_pair, _) = self.chip.masks.remap_to_global(col, 0)
                (region_col, region_row) = self.chip.masks.remap_to_registers(core_col, core_row, region,
                                                                              pixel_pair)
                self.chip.registers['REGION_COL'].write(int(region_col, 2))
                self.chip._read_register(1, write=True)
                self.chip._read_register(1, write=True)
                for row in range(rows):  # Loop over every row, write double pixels
                    (core_col, core_row, region, pixel_pair, _) = self.chip.masks.remap_to_global(col, row)
                    (region_col, region_row) = self.chip.masks.remap_to_registers(core_col, core_row, region,
                                                                                  pixel_pair)
                    indata += self.chip.registers['REGION_ROW'].get_write_command(int(region_row, 2))
                    if self.chip.chip_type.lower() == 'rd53a':  # special rd53a treatment
                        indata += self.chip.write_sync(write=False) * 20 * sync_multiplier
                        indata += self.chip.registers['REGION_ROW'].get_read_command() * sync_multiplier
                    indata += self.chip.write_sync() * 20 * sync_multiplier
                    indata += self.chip.registers['PIX_PORTAL'].get_read_command() * sync_multiplier
                    indata += self.chip.write_sync() * 20 * sync_multiplier
                    pbar.update(1)
                    if len(indata) > 2000:  # Write command to chip before it gets too long
                        self.chip.write_command(indata)
                        indata = self.chip.write_sync_01(write=False)
                self.chip.write_command(indata)
        pbar.close()

    def _scan(self, test_pattern=0b10101010, **_):
        '''
        Register test main loop

        Parameters
        ----------
        addresses : list
            List of registers to ignore
        '''
        cols = self.chip.masks.dimensions[0]  # 400
        rows = self.chip.masks.dimensions[1]  # 384 192

        for outer_loop in range(2):

            pixel_register = np.full((cols, rows), test_pattern)
            if self.chip.chip_type.lower() == 'rd53a':  # special sync fe treatment
                pixel_register[0:128, :] = test_pattern & 0b00000111

            self.write_pixels(pixel_register, outer_loop)
            self.read_pixels(outer_loop)
            test_pattern = (~np.uint8(test_pattern))

    def _analyze(self):
        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
            with tb.open_file(self.output_filename + '_interpreted.h5', 'w', title=in_file.title) as out_file:
                out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)

                out_file.create_group(out_file.root, name='chip_status', title='Chip Status')
                out_file.copy_children(in_file.root.chip_status, out_file.root.chip_status, recursive=True)

                raw_data = in_file.root.raw_data[:]
                meta_data = in_file.root.meta_data[:]

                pix_data0 = in_file.root.pix_data_0[:]
                pix_data1 = in_file.root.pix_data_1[:]

                out_file.create_carray(out_file.root, name='pix_data_0', title='Pixel Data', obj=pix_data0,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='pix_data_1', title='Pixel Data', obj=pix_data1,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                chip_mask = in_file.root.configuration_out.chip.use_pixel[:]
                run_config = au.ConfigDict(in_file.root.configuration_in.chip.settings[:])

                cols = chip_mask.shape[0]
                rows = chip_mask.shape[1]

                comparing_array = np.zeros((cols, rows))
                for outer_loop in range(2):
                    pixel_register = np.full((cols, rows), np.nan)
                    current_meta = meta_data[np.where(meta_data['scan_param_id'] == outer_loop)]
                    current_raw = raw_data[current_meta['index_start'][0]:current_meta['index_stop'][-1]]
                    if run_config['chip_type'].lower() == 'itkpixv1':
                        interpreted_userk = anb.interpret_userk_data(current_raw)
                    if run_config['chip_type'].lower() == 'rd53a':
                        interpreted_userk = ana.interpret_userk_data(current_raw)
                    row0, row1 = 0, 0
                    old_row = rows - 1
                    current_col = 0
                    for cnt, entries in enumerate(interpreted_userk):
                        if run_config['chip_type'].lower() == 'rd53a':
                            if old_row == (rows - 1) and (entries['Data0_Addr'] == 1 or entries['Data1_Addr'] == 1):
                                if entries['Data0_Addr'] == 1:
                                    current_col = entries['Data0_Data']
                                if entries['Data1_Addr'] == 1:
                                    current_col = entries['Data1_Data']
                            if entries['Data0_Addr'] == 2 or entries['Data1_Addr'] == 2:
                                if entries['Data0_Addr'] == 2 and entries['Data0_Data'] < rows:
                                    row0 = entries['Data0_Data']
                                elif old_row in [2]:
                                    pixel_register[current_col * 2, 2] = entries['Data0_Data'] >> 8
                                    pixel_register[current_col * 2 + 1, 2] = entries['Data0_Data'] & 0xff
                                if entries['Data1_Addr'] == 2 and entries['Data1_Data'] < rows:
                                    row1 = entries['Data1_Data']
                                elif old_row in [2]:
                                    pixel_register[current_col * 2, 2] = entries['Data1_Data'] >> 8
                                    pixel_register[current_col * 2 + 1, 2] = entries['Data1_Data'] & 0xff
                            else:
                                if entries['Data0_Addr'] == row0 or entries['Data0_Addr'] == row1:
                                    if entries['Data0_Addr'] == row0:
                                        pixel_register[current_col * 2, row0] = entries['Data0_Data'] >> 8
                                        pixel_register[current_col * 2 + 1, row0] = entries['Data0_Data'] & 0xff
                                    if entries['Data0_Addr'] == row1:
                                        pixel_register[current_col * 2, row1] = entries['Data0_Data'] >> 8
                                        pixel_register[current_col * 2 + 1, row1] = entries['Data0_Data'] & 0xff

                                if entries['Data1_Addr'] == row0 or entries['Data1_Addr'] == row1:
                                    if entries['Data1_Addr'] == row0:
                                        pixel_register[current_col * 2, row0] = entries['Data1_Data'] >> 8
                                        pixel_register[current_col * 2 + 1, row0] = entries['Data1_Data'] & 0xff
                                    if entries['Data1_Addr'] == row1:
                                        pixel_register[current_col * 2, row1] = entries['Data1_Data'] >> 8
                                        pixel_register[current_col * 2 + 1, row1] = entries['Data1_Data'] & 0xff

                            if row0 > row1:
                                old_row = row0
                            if row1 >= row0:
                                old_row = row1
                        elif run_config['chip_type'].lower() == 'itkpixv1':
                            if (entries['Data1_Addr'] == 1 and entries['Data0_Addr'] == 511) and entries['Data1_Data'] < rows:
                                ccol = entries['Data1_Data']
                            elif (entries['Data1_Addr'] == 511 and entries['Data0_Addr'] == 1) and entries['Data0_Data'] < rows:
                                ccol = entries['Data0_Data']
                            else:
                                if entries['Data1_Addr'] != 511:
                                    pixel_register[ccol * 2, entries['Data1_Addr']] = entries['Data1_Data'] >> 8
                                    pixel_register[ccol * 2 + 1, entries['Data1_Addr']] = entries['Data1_Data'] & 0xff
                                if entries['Data0_Addr'] != 511:
                                    pixel_register[ccol * 2, entries['Data0_Addr']] = entries['Data0_Data'] >> 8
                                    pixel_register[ccol * 2 + 1, entries['Data0_Addr']] = entries['Data0_Data'] & 0xff

                    out_file.create_carray(out_file.root, name='pix_data_analyzed_' + str(outer_loop),
                                           title='Pixel Data analyzed ' + str(outer_loop),
                                           obj=pixel_register,
                                           filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    if outer_loop == 0:
                        comparing_array[np.where(pixel_register == pix_data0)] = 1
                    if outer_loop == 1:
                        comparing_array[np.where(pixel_register == pix_data1)] = 1

                out_file.create_carray(out_file.root, name='pix_data_working', title='Pixel Data working',
                                       obj=comparing_array,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=self.output_filename + '_interpreted.h5') as p:
                p.create_parameter_page()
                p._plot_occupancy(hist=comparing_array.T,
                                  title='Map of Working Pixel Registers',
                                  z_label='Working = 1',
                                  z_max=1,
                                  show_sum=True)


if __name__ == '__main__':
    with PixelRegisterScan(scan_config=scan_configuration) as scan:
        scan.start()
