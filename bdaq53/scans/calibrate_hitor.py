#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
   Creates the hitor calibration per pixel using the charge injection cicuit. In order to prevent wrong TDC measurements
   (e.g. due to AFE overshoot, ...) only TDC words are written to the data stream if a valid (in time) trigger is measured
   with the hit (HitOr output). For this, TX0 has to be connected with RX0 (as short as possible) in order to connect
   the trigger signal (CMD_LOOP_START_PULSE) to the trigger input of the TDC module. Check the wiki for a detailed instruction.
   If the hit delay is not within the measurement range (255 x 1.5625 ns) adjust the delay of pulser_cmd_start_loop.
   If a hit delay measurement is not needed, set the `EN_TRIGGER_DIST` register of the TDC module to zero.
'''

from tqdm import tqdm
import numpy as np
import tables as tb
from scipy import interpolate

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject, get_scan_loop_mask_steps
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import analysis_utils as au

scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'VCAL_MED': 500,
    'VCAL_HIGH_values': range(900, 2001, 100)
}


class HitorCalib(ScanBase):
    scan_id = 'hitor_calibration'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['hitbus'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update(force=True)

        # Start CMD LOOP PULSER, 160 MHz
        self.bdaq['pulser_cmd_start_loop'].set_en(True)
        self.bdaq['pulser_cmd_start_loop'].set_width(400)
        self.bdaq['pulser_cmd_start_loop'].set_delay(80)
        self.bdaq['pulser_cmd_start_loop'].set_repeat(1)
        # Configure LEMO mux such that CMD_LOOP_SIGNAL is assigned to LEMO_TX0
        self.bdaq.set_LEMO_MUX(connector='LEMO_MUX_TX0', value=1)

        # Configure all four TDC modules
        self.bdaq.configure_tdc_modules()

    def _scan(self, n_injections=100, VCAL_MED=500, VCAL_HIGH_values=range(800, 4001, 200), **_):
        '''
        Calibrate hitor main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_values : list
            List of VCAL_HIGH DAC values to scan.
            This API allows for a list of arbitrary values since, e.g. the response of DIFF is not linear with charge.
        '''

        values = VCAL_HIGH_values
        pattern = 'hitbus'
        masks = ['enable', 'injection', 'hitbus']
        pbar = tqdm(total=get_scan_loop_mask_steps(scan=self, pattern=pattern) * len(values), unit=' Mask steps')

        self.enable_hitor(True)
        self.bdaq.enable_tdc_modules()

        for scan_param_id, value in enumerate(values):
            self.chip.setup_analog_injection(vcal_high=value, vcal_med=VCAL_MED)
            self.store_scan_par_values(scan_param_id=scan_param_id, vcal_high=value, vcal_med=VCAL_MED)
            with self.readout(scan_param_id=scan_param_id):
                shift_and_inject(scan=self, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id, masks=masks, pattern=pattern)

        self.bdaq.disable_tdc_modules()
        self.enable_hitor(False)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        def _mask_disabled_pixels(enable_mask, scan_config):
            mask = np.invert(enable_mask)
            mask[:scan_config['start_column'], :] = True
            mask[scan_config['stop_column']:, :] = True
            mask[:, :scan_config['start_row']] = True
            mask[:, scan_config['stop_row']:] = True
            return mask

        self.configuration['bench']['analysis']['store_hits'] = True
        self.configuration['bench']['analysis']['analyze_tdc'] = True
        self.configuration['bench']['analysis']['use_tdc_trigger_dist'] = True
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            max_scan_param_id = a.get_scan_param_values(scan_parameter='vcal_high').shape[0]
            chunk_size = a.chunk_size
            tot_max = 512 if a.analyze_ptot else 16
            tdc_max = 500
            n_cols = a.columns
            n_rows = a.rows

        # Create col, row, tot histograms from hits
        with tb.open_file(self.output_filename + '_interpreted.h5', 'r+') as io_file:
            hist_tot_mean = np.zeros(shape=(n_cols, n_rows, max_scan_param_id), dtype=np.float32)
            hist_tdc_mean = np.zeros(shape=(n_cols, n_rows, max_scan_param_id), dtype=np.float32)
            hist_tot_std = np.zeros(shape=(n_cols, n_rows, max_scan_param_id), dtype=np.float32)
            hist_tdc_std = np.zeros(shape=(n_cols, n_rows, max_scan_param_id), dtype=np.float32)

            bin_positions_tot = np.tile(np.arange(tot_max), (n_cols, n_rows)).reshape(n_cols, n_rows, tot_max)
            bin_positions_tdc = np.tile(np.arange(tdc_max), (n_cols, n_rows)).reshape(n_cols, n_rows, tdc_max)
            hist_2d_tdc_vcal = np.zeros(shape=(max_scan_param_id, tdc_max), dtype=np.float32)

            old_scan_par = -1
            is_new_scan_par = True
            hist_tot = np.zeros(shape=(n_cols, n_rows, tot_max))
            hist_tdc = np.zeros(shape=(n_cols, n_rows, tdc_max))
            self.log.info('Creating Histograms...')
            # Yield all hits from the same scan parameter. Analysis allows that one scan parameter can be yieled more than once
            pbar = tqdm(total=max_scan_param_id, unit=' Scan parameters')
            for par, hits in au.hits_of_parameter(hits=io_file.root.Hits, chunk_size=chunk_size):
                # Save mean and std of TOT and TDC for each pixel of actual scan parameter
                if par != old_scan_par and old_scan_par != -1:
                    hist_tot_mean[:, :, old_scan_par] = au.get_mean_from_histogram(hist_tot, bin_positions=np.arange(tot_max), axis=2)
                    hist_tdc_mean[:, :, old_scan_par] = au.get_mean_from_histogram(hist_tdc, bin_positions=np.arange(tdc_max), axis=2)
                    hist_tot_std[:, :, old_scan_par] = au.get_std_from_histogram(hist_tot, bin_positions_tot, axis=2)
                    hist_tdc_std[:, :, old_scan_par] = au.get_std_from_histogram(hist_tdc, bin_positions_tdc, axis=2)
                    is_new_scan_par = True
                    pbar.update(1)

                # Select only good tdc values
                selection = np.logical_and(hits['tdc_status'] == 1, hits['tdc_value'] < tdc_max)
                hits = hits[selection]
                tdc_value = hits['tdc_value']
                tot_value = hits['ptot'] if a.analyze_ptot else hits['tot']
                scan_param_id = hits['scan_param_id']
                col = hits['col']
                row = hits['row']

                if is_new_scan_par:
                    # Histogram for each pixel TOT and TDC
                    hist_tot = au.hist_3d_index(col, row, tot_value, shape=(n_cols, n_rows, tot_max))
                    hist_tdc = au.hist_3d_index(col, row, tdc_value, shape=(n_cols, n_rows, tdc_max))
                    is_new_scan_par = False
                else:
                    hist_tot += au.hist_3d_index(col, row, tot_value, shape=(n_cols, n_rows, tot_max))
                    hist_tdc += au.hist_3d_index(col, row, tdc_value, shape=(n_cols, n_rows, tdc_max))

                # Histogram delta vcal values and tdc value
                x_edges = range(0, max_scan_param_id + 1)
                y_edges = range(0, tdc_max + 1)
                hist, _, _ = np.histogram2d(x=scan_param_id,
                                            y=tdc_value,
                                            bins=(x_edges, y_edges))
                hist_2d_tdc_vcal += hist
                old_scan_par = par

            # Save mean and std of TOT and TDC for each pixel of last scan parameter
            hist_tot_mean[:, :, par] = au.get_mean_from_histogram(hist_tot, bin_positions=np.arange(tot_max), axis=2)
            hist_tdc_mean[:, :, par] = au.get_mean_from_histogram(hist_tdc, bin_positions=np.arange(tdc_max), axis=2)
            hist_tot_std[:, :, par] = au.get_std_from_histogram(hist_tot, bin_positions_tot, axis=2)
            hist_tdc_std[:, :, par] = au.get_std_from_histogram(hist_tdc, bin_positions_tdc, axis=2)
            pbar.update(1)
            pbar.close()

            def create_lookup_table():

                ''' Create lookup table (TDC to DeltaVCAL) for each pixel using interpolation.
                '''

                scan_config = au.ConfigDict(io_file.root.configuration_in.scan.scan_config[:])
                charge_dvcal_values = io_file.root.configuration_in.scan.scan_params[:]['vcal_high'] - io_file.root.configuration_in.scan.scan_params[:]['vcal_med']
                enable_mask = ~_mask_disabled_pixels(io_file.root.configuration_in.chip.use_pixel[:], scan_config)
                start_column = scan_config['start_column']
                stop_column = scan_config['stop_column']
                start_row = scan_config['start_row']
                stop_row = scan_config['stop_row']
                failed_interpolations = 0

                # Lookup table: Delta VCAL values for all TDC values until max TDC in steps of 1 for each pixel
                look_up_table = np.full_like(hist_tdc, fill_value=np.nan, dtype=np.float32)

                self.log.info('Interpolation Starting...')
                for col in range(start_column, stop_column):
                    for row in range(start_row, stop_row):
                        if enable_mask[col, row]:
                            tdc_values = hist_tdc_mean[col, row, :]
                            selection = ~np.isnan(tdc_values)
                            try:
                                # It was observed that first degree interpolation gives more robust result between consecutive points while second degree some time induces weird behavior
                                dvcal = np.arange(np.min(charge_dvcal_values[selection]), np.max(charge_dvcal_values[selection]), 1)
                                spline = interpolate.splrep(charge_dvcal_values[selection], tdc_values[selection], s=0, k=1)  # create spline interpolation
                                spline_eval = interpolate.splev(dvcal, spline)  # evaluate spline
                                charge_dvcal_interpolation = np.interp(np.arange(look_up_table.shape[-1]), spline_eval, dvcal)  # create evaluation for every TDC value
                                look_up_table[col, row, int(spline_eval[0]):int(spline_eval[-1])] = charge_dvcal_interpolation[int(spline_eval[0]):int(spline_eval[-1])]  # only fill lookup table from lowest upto highest measured TDC value
                            except(TypeError, ValueError):  # in case interpolation failed
                                failed_interpolations += 1
                                self.log.debug('Could not do interpolation for pixel (%i, %i)' % (col, row))

                self.log.info('%i Interpolations out of %i failed.' % (failed_interpolations, np.count_nonzero(enable_mask)))

                return look_up_table

            lookup_table = create_lookup_table()
            # Store histograms
            io_file.create_carray(io_file.root,
                                  name='hist_2d_tdc_vcal',
                                  title='2D Hist TDC vs DeltaVCAL',
                                  obj=hist_2d_tdc_vcal,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            io_file.create_carray(io_file.root,
                                  name='hist_tot_mean',
                                  title='Mean tot calibration histogram',
                                  obj=hist_tot_mean,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            io_file.create_carray(io_file.root,
                                  name='hist_tdc_mean',
                                  title='Mean Tdc calibration histogram',
                                  obj=hist_tdc_mean,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))
            io_file.create_carray(io_file.root,
                                  name='hist_tot_std',
                                  title='Std tot calibration histogram',
                                  obj=hist_tot_std,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            io_file.create_carray(io_file.root,
                                  name='hist_tdc_std',
                                  title='Std Tdc calibration histogram',
                                  obj=hist_tdc_std,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            io_file.create_carray(io_file.root,
                                  name='lookup_table',
                                  title=r'Lookup table for TDC to DeltaVCAL conversion',
                                  obj=lookup_table,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

        with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
            p.create_standard_plots()


if __name__ == '__main__':
    with HitorCalib(scan_config=scan_configuration) as calibration:
        calibration.start()
