#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This calibration routine calibrates the on chip temperature
    sensors to the NTC on the singe chip card.
    Please make sure you have mounted the correct resistors
    to your bdaq board and activated the NTC in the testbench.yaml.
'''

import numpy as np
import tables as tb
from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase

scan_configuration = {
    'samples': 10
}


class TempSensorCalib(ScanBase):
    scan_id = "calibrate_temperature"

    def _scan(self, samples=10, diode_current=14, **kwargs):
        '''
        Temperature calibration main loop

        Parameters
        ----------
        samples : int [1:inf]
            Temperature samples to be taken to maximize statistics
        '''

        sensor_array = ['TEMPSENS_1', 'TEMPSENS_2', 'TEMPSENS_3', 'TEMPSENS_4']

        rows = []
        scan_param_id = 0
        pbar = tqdm(total=len(sensor_array) * 2 * samples, unit=' Steps')
        for sensor_number, sensor in enumerate(sensor_array):
            for reps in range(samples):
                for sen_sel in range(0, 2, 1):
                    bitstring = int('1' + format(diode_current, '04b') + format(sen_sel, '01b') + '1' + format(diode_current, '04b') + format(sen_sel, '01b'), 2)
                    self.chip._write_register(99, bitstring, write=True)
                    self.chip._write_register(100, bitstring, write=True)
                    with self.readout(scan_param_id=scan_param_id):
                        val_adc, _ = self.chip.get_ADC_value(sensor, measure='v')
                        rows.append((scan_param_id, sensor_number, self.chip.get_temperature_NTC(log=False), sen_sel, val_adc, diode_current))
                        scan_param_id += 1
                        pbar.update()

        self.data.temp_data = np.zeros(0, dtype={'names': ['measurement', 'sensor_number', 'bdaq_temp', 'sensor_config', 'ADC', 'sensor_current'],
                                                 'formats': ['int32', 'int32', 'float32', 'int32', 'int32', 'int32']})
        self.data.temp_data_table = self.h5_file.create_table(self.h5_file.root, name='temp_data', title='temp_data', description=self.data.temp_data)
        row = self.data.temp_data_table.row
        for scan_param_id, sensor_number, temperature, sen_sel, val_adc, diode_current in rows:
            row['measurement'] = scan_param_id
            row['sensor_number'] = sensor_number
            row['bdaq_temp'] = temperature
            row['sensor_config'] = sen_sel
            row['ADC'] = val_adc
            row['sensor_current'] = diode_current
            row.append()
            self.data.temp_data_table.flush()

    def _analyze(self):
        def get_Nf(temp, dV):
            return self.chip.calibration.e * dV / ((temp + 273.15) * self.chip.calibration.kb * np.log(15))

        def get_dNf(temp, dtemp, dv, ddv):
            return self.chip.calibration.e / (self.chip.calibration.kb * np.log(15)) * \
                np.sqrt((ddv / (temp - 273.15)) ** 2 + ((dtemp) * dv / (temp - 273.15) ** 2) ** 2)

        def get_dtemp(dV, ddV, Nf, dNf):
            return self.chip.calibration.e / (self.chip.calibration.kb * np.log(15)) * \
                np.sqrt((ddV / Nf) ** 2 + (dV * dNf / Nf ** 2) ** 2)

        with tb.open_file(self.output_filename + '_interpreted.h5', 'w') as out_file:
            with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
                temp_data = in_file.root.temp_data[:]
                out_file.copy_node(in_file.root.temp_data, out_file.root)
                out_file.copy_node(in_file.root.configuration, out_file.root, recursive=True)

            Nf_data = np.zeros(4, dtype={'names': ['sensor_number', 'dADC', 'dADC_std', 'temp_avg', 'temp_std', 'Nf', 'Nf_std', 'chip_temp', 'temp_accuracy'],
                                         'formats': ['int32', 'float32', 'float32', 'float32', 'float32', 'float32', 'float32', 'float32', 'float32']})

            for sensor in range(4):
                indices_c0 = np.where((temp_data['sensor_number'] == sensor) & (temp_data['sensor_config'] == 0))[0]
                indices_c1 = np.where((temp_data['sensor_number'] == sensor) & (temp_data['sensor_config'] == 1))[0]
                dADC = np.average(temp_data[indices_c1]['ADC'] - temp_data[indices_c0]['ADC'])
                dADC_std = np.std(temp_data[indices_c1]['ADC'] - temp_data[indices_c0]['ADC'])
                temp_array = np.hstack((temp_data[indices_c0]['bdaq_temp'], temp_data[indices_c1]['bdaq_temp']))
                temp = np.average(temp_array)
                temp_std = np.std(temp_array)
                Nf_data[sensor]['sensor_number'] = sensor
                Nf_data[sensor]['dADC'] = (dADC)
                Nf_data[sensor]['dADC_std'] = (dADC_std)
                Nf_data[sensor]['temp_avg'] = (temp)
                Nf_data[sensor]['temp_std'] = (temp_std)
                Nf_data[sensor]['Nf'] = (get_Nf(temp, self.chip.calibration.get_V_from_ADC(dADC, False)))
                Nf_data[sensor]['Nf_std'] = (get_dNf(temp, temp_std, self.chip.calibration.get_V_from_ADC(dADC, False),
                                                     self.chip.calibration.get_V_from_ADC(dADC_std, False)))
                Nf_data[sensor]['chip_temp'] = self.chip.calibration.get_temperature_from_ADC(dADC, sensor=sensor)
                Nf_data[sensor]['temp_accuracy'] = get_dtemp(self.chip.calibration.get_V_from_ADC(dADC, False),
                                                             self.chip.calibration.get_V_from_ADC(dADC_std, False),
                                                             Nf_data[sensor]['Nf'],
                                                             Nf_data[sensor]['Nf_std'])

            out_file.create_table(out_file.root, 'Nf_data', Nf_data, title='Nf_data', filters=tb.Filters(complib='blosc',
                                                                                                         complevel=5,
                                                                                                         fletcher32=False))
            self.log.success('The ideal Nf factors are:\nNf_0: {0:1.4f}\nNf_1: {1:1.4f}\nNf_2: {2:1.4f}\nNf_3: {3:1.4f}\n'
                             'Please enter these values in your chip configuration file!'.format(Nf_data[0]['Nf'], Nf_data[1]['Nf'],
                                                                                                 Nf_data[2]['Nf'], Nf_data[3]['Nf']))


if __name__ == "__main__":
    with TempSensorCalib(scan_config=scan_configuration) as calibration:
        calibration.start()
