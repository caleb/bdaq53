#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Tune global and local threshold and ToT.
    Run a threshold scan to visualize the result.
'''

from bdaq53.scans.tune_global_threshold import GDACTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.scans.tune_tot import TotTuning
from bdaq53.scans.scan_noise_occupancy import NoiseOccScan
from bdaq53.scans.scan_stuck_pixel import StuckPixelScan
from bdaq53.scans.scan_threshold import ThresholdScan
from bdaq53.scans.scan_analog import AnalogScan
from bdaq53.scans.scan_digital import DigitalScan

scan_configuration_flavor_analog = {
    "sync": {
        "start_column": 0,
        "stop_column": 128,
        "start_row": 0,
        "stop_row": 192,
        # Target threshold
        "VCAL_MED": 500,  # 180 DVCAL corresponds to about 2000 e
        "VCAL_HIGH": 1100,
    },
    "lin": {
        "start_column": 128,
        "stop_column": 264,
        "start_row": 0,
        "stop_row": 192,
        # Target threshold
        "VCAL_MED": 500,
        "VCAL_HIGH": 1100,
    },
    "diff": {
        "start_column": 264,
        "stop_column": 400,
        "start_row": 0,
        "stop_row": 192,
        # Target threshold
        "VCAL_MED": 500,
        "VCAL_HIGH": 1100,
    },
}

tot_tuning_configuration = {
    "target_tot": {
        "SYNC": 7,
        "LIN": 7,
        "DIFF": 7,
    },  # target value for each FE to which ToT is tuned
    "delta_tot": 0.2,  # max. difference between target ToT and tuned ToT value for each FE
    "lower_limit": {
        "SYNC": 20,
        "LIN": 10,
        "DIFF": 20,
    },  # lower bound for the feedback register value for each FE
    "upper_limit": {
        "SYNC": 160,
        "LIN": 130,
        "DIFF": 120,
    },  # upper bound for the feedback register value for each FE
    "max_iterations": 10,  # max. number of iterations after which tuning is aborted
    "VCAL_MED": 500,
    "VCAL_HIGH": 1100,
}

noise_occ_scan_configuration = {
    # Start the scan with a fresh disable mask (resets pixels disabled by other scans)?
    "reset_disable_mask": True,
    # 'max_occupancy': 1e-6,  # Maximum noise occupancy per bunch crossing
    "n_triggers": 1e7,  # Total number of triggers which are send
    "min_occupancy": 10,  # All pixels with more hits than this threshold are masked as noisy
    "certainty": 2.0,  # Expected/tolerable number of wrongly classified pixels (noisy vs. quiet). Note that amount of noise changes with time on a real chip.
    "stop_timeout": 20,  # Scan stops if number of unclassified pixels did not change for stop_timeout seconds. Hard cut is applied to remaining pixels.
    "max_scan_time": 120,  # Maximum scan time to prevent not terminating scan. Hard cut is applied to remaining pixels.
}

scan_configuration_flavor = {
    "sync": {
        "start_column": 0,
        "stop_column": 128,
        "start_row": 0,
        "stop_row": 192,
        # Target threshold
        "VCAL_MED": 500,
        "VCAL_HIGH": 730,
    },
    "lin": {
        "start_column": 128,
        "stop_column": 264,
        "start_row": 0,
        "stop_row": 192,
        # Target threshold
        "VCAL_MED": 500,
        "VCAL_HIGH": 680,
    },
    "diff": {
        "start_column": 264,
        "stop_column": 400,
        "start_row": 0,
        "stop_row": 192,
        # Target threshold
        "VCAL_MED": 500,
        "VCAL_HIGH": 630,
    },
}

if __name__ == "__main__":
    # tune linFE to 2ke
    with GDACTuning(scan_config=scan_configuration_flavor["lin"]) as global_tuning:
        global_tuning.start()
    with TDACTuning(scan_config=scan_configuration_flavor["lin"]) as local_tuning:
        local_tuning.start()
    with AnalogScan(scan_config=scan_configuration_flavor["lin"]) as analog_scan:
        analog_scan.start()

    scan_configuration_flavor["lin"]["maskfile"] = "auto"  # Definitely use maskfile created by tuning from here on
    tot_params = scan_configuration_flavor["lin"].copy()
    tot_params.update(tot_tuning_configuration)
    with TotTuning(scan_config=tot_params) as tot_tuning:
        tot_tuning.start()
    with AnalogScan(scan_config=scan_configuration_flavor_analog["lin"]) as analog_scan:
        analog_scan.start()

    # tune syncFE target 2.5 ke, linFE tune to 1.5 ke, diffFE target 1.5 ke
    scan_configuration_flavor["lin"]["VCAL_HIGH"] -= 50
    for flavor in scan_configuration_flavor:
        with GDACTuning(scan_config=scan_configuration_flavor[flavor]) as global_tuning:
            global_tuning.start()
        with TDACTuning(scan_config=scan_configuration_flavor[flavor]) as local_tuning:
            local_tuning.start()
        with AnalogScan(scan_config=scan_configuration_flavor_analog[flavor]) as analog_scan:
            analog_scan.start()

        scan_configuration_flavor[flavor]["maskfile"] = "auto"  # Definitely use maskfile created by tuning from here on
        tot_params = scan_configuration_flavor[flavor].copy()
        tot_params.update(tot_tuning_configuration)
        with TotTuning(scan_config=tot_params) as tot_tuning:
            tot_tuning.start()

        with AnalogScan(scan_config=scan_configuration_flavor_analog[flavor]) as analog_scan:
            analog_scan.start()

        with TDACTuning(scan_config=scan_configuration_flavor[flavor]) as local_tuning:
            local_tuning.start()

        with AnalogScan(scan_config=scan_configuration_flavor_analog[flavor]) as analog_scan:
            analog_scan.start()
        noise_occ_scan_configuration["start_column"] = scan_configuration_flavor[flavor]["start_column"]
        noise_occ_scan_configuration["stop_column"] = scan_configuration_flavor[flavor]["stop_column"]
        noise_occ_scan_configuration["start_row"] = scan_configuration_flavor[flavor]["start_row"]
        noise_occ_scan_configuration["stop_row"] = scan_configuration_flavor[flavor]["stop_row"]

        # First noise occupancy scan
        with NoiseOccScan(scan_config=noise_occ_scan_configuration) as noise_occ_scan:
            noise_occ_scan.start()
        # only do a Stuck Pixel scan if not only the sync flavor is used!
        if flavor != "sync":
            with StuckPixelScan(scan_config=scan_configuration_flavor[flavor]) as stuck_pix_scan:
                stuck_pix_scan.start()
        # Second noise occupancy scan, find more noisy pixels
        noise_occ_scan_configuration["reset_disable_mask"] = False
        with NoiseOccScan(scan_config=noise_occ_scan_configuration) as noise_occ_scan:
            noise_occ_scan.start()

    # Calculate scan range for threshold scan
        target_threshold = scan_configuration_flavor[flavor]["VCAL_HIGH"] - scan_configuration_flavor[flavor]["VCAL_MED"]
        scan_configuration_flavor[flavor]["VCAL_HIGH_start"] = scan_configuration_flavor[flavor]["VCAL_MED"] + target_threshold - 40
        scan_configuration_flavor[flavor]["VCAL_HIGH_stop"] = scan_configuration_flavor[flavor]["VCAL_MED"] + target_threshold + 40
        scan_configuration_flavor[flavor]["VCAL_HIGH_step"] = 2
        # do a threshhold scan, an analog scan & a digital scan
        with ThresholdScan(scan_config=scan_configuration_flavor[flavor]) as thr_scan:
            thr_scan.start()
        with AnalogScan(scan_config=scan_configuration_flavor_analog[flavor]) as analog_scan:
            analog_scan.start()
        with DigitalScan(scan_config=scan_configuration_flavor[flavor]) as digital_scan:
            digital_scan.start()
