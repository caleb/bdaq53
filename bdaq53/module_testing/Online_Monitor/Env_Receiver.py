# from PyQt5 import Qt
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
from pyqtgraph.dockarea import DockArea, Dock
from datetime import datetime
import numpy as np

from online_monitor.utils import utils
from online_monitor.receiver.receiver import Receiver


class TimeAxisItem(pg.AxisItem):
    def tickStrings(self, values, scale, spacing):
        return [datetime.fromtimestamp(value).strftime("%x %X") for value in values]


class Env_Receiver(Receiver):
    def setup_receiver(self):
        self.set_bidirectional_communication()  # We want to change converter settings
        self.avg_window = 180

    def setup_widgets(self, parent, name):
        dock_area = DockArea()
        parent.addTab(dock_area, name)
        # Docks
        dock_values = Dock("Temperature and Humidity", size=(400, 400))
        dock_status = Dock("Status", size=(800, 40))
        dock_area.addDock(dock_values, "above")
        dock_area.addDock(dock_status, "top")
        cw = QtGui.QWidget()
        cw.setStyleSheet("QWidget {background-color:white}")
        layout = QtGui.QGridLayout()
        cw.setLayout(layout)
        self.dewpoint_label = QtGui.QLabel("Dew point:\n-- C")
        self.last_timestamp_label = QtGui.QLabel("Last timestamp:\n%s" % "no")
        layout.addWidget(self.dewpoint_label, 0, 2, 1, 1)
        layout.addWidget(self.last_timestamp_label, 0, 3, 1, 1)
        layout.addWidget(self.reset_button, 0, 4, 1, 1)
        dock_status.addWidget(cw)

        # Connect widgets
        self.reset_button.clicked.connect(lambda: self.send_command("RESET"))
        # particle rate dock
        cooling_graphics = pg.GraphicsLayoutWidget()
        cooling_graphics.show()
        # Temperature plots
        date_axis_temp = TimeAxisItem(orientation="bottom")
        date_axis_humid = TimeAxisItem(orientation="bottom")
        plot_temp = pg.PlotItem(axisItems={"bottom": date_axis_temp}, labels={"left": "Temperature / °C"})
        plot_humidity = pg.PlotItem(axisItems={"bottom": date_axis_humid}, labels={"left": "rel. Humidity / %"})
        plot_humidity.setXLink(plot_temp)
        self.temp_sensor_curve_1 = pg.PlotCurveItem(pen=pg.mkPen("b", width=2))
        self.temp_sensor_curve_2 = pg.PlotCurveItem(pen=pg.mkPen("g", width=2))
        self.humid_sensor_curve_1 = pg.PlotCurveItem(pen=pg.mkPen("r", width=2))
        self.temp_NTC_curve = pg.PlotCurveItem(pen=pg.mkPen(0, 181, 97), width=2)
        self.dewpoint_curve_1 = pg.PlotCurveItem(pen=pg.mkPen("r", width=2))
        self.dewpoint_curve_2 = pg.PlotCurveItem(pen=pg.mkPen("y", width=2))
        # add legend
        legend_temp = pg.LegendItem(offset=(50, 1))
        legend_temp.setParentItem(plot_temp)
        legend_temp.addItem(self.temp_NTC_curve, r"$T_\mathrm{M}$")  # module temperature
        legend_temp.addItem(self.temp_sensor_curve_1, r"$T_\mathrm{A}$")  # temperatre in testing box
        legend_temp.addItem(self.temp_sensor_curve_2, r"$T_\mathrm{B}$")  # temperature on cooling block
        legend_temp.addItem(self.dewpoint_curve_1, r"T_\mathrm{DP1}")  # dewpoint calculated through T_A and RH
        legend_temp.addItem(self.dewpoint_curve_2, r"T_\mathrm{DP2}")  # dewpoint calculated through T_B and RH
        legend_humid_sensor = pg.LegendItem(offset=(50, 1))
        legend_humid_sensor.setParentItem(plot_humidity)
        legend_humid_sensor.addItem(self.humid_sensor_curve_1, "RH")  # relative humidity
        # add items to plots and customize plots viewboxes
        plot_temp.addItem(self.temp_sensor_curve_1)
        plot_temp.addItem(self.temp_sensor_curve_2)
        plot_temp.addItem(self.dewpoint_curve_1)
        plot_temp.addItem(self.dewpoint_curve_2)
        plot_temp.addItem(self.temp_NTC_curve)
        plot_temp.vb.setBackgroundColor("#E6E5F4")
        plot_temp.getAxis("left").setZValue(0)
        plot_temp.getAxis("left").setGrid(155)
        plot_temp.getAxis("bottom").setStyle(showValues=False)
        plot_humidity.addItem(self.humid_sensor_curve_1)
        # plot_humidity.addItem(self.humid_sensor_curve_2)
        plot_humidity.vb.setBackgroundColor("#E6E5F4")
        plot_humidity.showGrid(x=True, y=True)
        plot_humidity.getAxis("left").setZValue(0)
        plot_humidity.getAxis("bottom").setGrid(155)
        cooling_graphics.addItem(plot_temp, row=0, col=1, rowspan=1, colspan=3)
        cooling_graphics.addItem(plot_humidity, row=1, col=1, rowspan=1, colspan=3)
        dock_values.addWidget(cooling_graphics)
        self.plot_delay = 0

    def deserialize_data(self, data):
        _, meta = utils.simple_dec(data)
        return meta

    def handle_data_if_active(self, data):

        if "temp_1" in data:
            mask = ~np.isnan(data["temp_1"])
            self.temp_sensor_curve_1.setData(data["time"][mask], data["temp_1"][mask], autoDownsample=True)
        if "temp_2" in data:
            mask = ~np.isnan(data["temp_2"])
            self.temp_sensor_curve_2.setData(data["time"][mask], data["temp_2"][mask], autoDownsample=True)
        if "humidity_1" in data:
            mask = ~np.isnan(data["humidity_1"])
            self.humid_sensor_curve_1.setData(data["time"][mask], data["humidity_1"][mask], autoDownsample=True)
        if "temp_NTC" in data:
            mask = ~np.isnan(data["temp_NTC"])
            self.temp_NTC_curve.setData(data["time"][mask], data["temp_NTC"][mask], autoDownsample=True)
        if "dew_1" in data:
            mask = ~np.isnan(data["dew_1"])
            self.dewpoint_curve_1.setData(data["time"][mask], data["dew_1"][mask], autoDownsample=True)
        if "dew_2" in data:
            mask = ~np.isnan(data["dew_2"])
            self.dewpoint_curve_2.setData(data["time"][mask], data["dew_2"][mask], autoDownsample=True)
        self.dewpoint_label.setText("Average Dew point:\n%.1f C" % float(data["dew_2"][-1]))
        self.last_timestamp_label.setText("Last timestamp:\n%s" % datetime.fromtimestamp(data["last_timestamp"]).strftime("%x %X"))

    def _update_avg_window(self, value):
        self.avg_window = value
        self.send_command(str(value))
