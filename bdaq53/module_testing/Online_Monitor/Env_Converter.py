import numpy as np

from online_monitor.utils import utils
from online_monitor.converter.transceiver import Transceiver


class Env_Converter(Transceiver):
    def setup_transceiver(self):
        """ Called at the beginning
            We want to be able to change the histogrammmer settings
            thus bidirectional communication needed
        """
        self.set_bidirectional_communication()

    def deserialize_data(self, data):
        dat = utils.simple_dec(data)
        return dat

    def setup_interpretation(self):
        # Maximum amount of values before override
        self.n_values = 7200
        self.last_timestamp = 0

        # Init result hists
        self.reset()

    def interpret_data(self, data):
        data, meta_data = data[0][1]
        self.update_arrays(data, meta_data)
        # Calculate recent average of curves (timespan can be defined in GUI)
        interpreted_data = {
            "temp_1": self.temp_arrays,
            "temp_2": self.temp_arrays_2,
            "humidity_1": self.humidity_arrays,
            "humidity_2": self.humidity_arrays_2,
            "temp_NTC" : self.temp_NTC_array,
            "dew_1": self.dewpoint,
            "dew_2": self.dewpoint_2,
            "time": self.timestamps,
            "last_timestamp": self.last_timestamp,
        }

        return [interpreted_data]

    def serialize_data(self, data):
        return utils.simple_enc(None, data)

    def handle_command(self, command):
        # received signal is 'ACTIVETAB tab' where tab is the name (str) of the selected tab in online monitor
        if command[0] == "RESET":
            self.reset()
        else:
            self.avg_window = int(command[0])

    def reset(self):
        self.temp_arrays = np.full(self.n_values, np.nan)
        self.humidity_arrays = np.full(self.n_values, np.nan)
        self.timestamps = np.full(self.n_values, np.nan)
        self.dewpoint = np.full(self.n_values, np.nan)
        self.temp_arrays_2 = np.full(self.n_values, np.nan)
        self.humidity_arrays_2 = np.full(self.n_values, np.nan)
        self.dewpoint_2 = np.full(self.n_values, np.nan)
        self.temp_NTC_array = np.full(self.n_values, np.nan)

        self.last_timestamp = 0

    def update_arrays(self, data, meta_data):
        self.temp_arrays = np.roll(self.temp_arrays, -1)
        self.temp_arrays[-1] = data[0]

        self.temp_arrays_2 = np.roll(self.temp_arrays_2, -1)
        self.temp_arrays_2[-1] = data[1]

        self.humidity_arrays = np.roll(self.humidity_arrays, -1)
        self.humidity_arrays[-1] = data[2]

        self.humidity_arrays_2 = np.roll(self.humidity_arrays_2, -1)
        self.humidity_arrays_2[-1] = data[3]

        self.dewpoint = np.roll(self.dewpoint, -1)
        self.dewpoint[-1] = data[4]
        self.dewpoint_2 = np.roll(self.dewpoint_2, -1)
        self.dewpoint_2[-1] = data[5]

        self.temp_NTC_array = np.roll(self.temp_NTC_array, -1)
        self.temp_NTC_array[-1] = data[6]

        self.timestamps = np.roll(self.timestamps, -1)
        self.timestamps[-1] = meta_data["timestamp"]
        self.last_timestamp = meta_data["timestamp"]
