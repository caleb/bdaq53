import time
import yaml
import os
import requests
from online_monitor.utils import utils
import tables as tb
import numpy as np
import zmq
from threading import Thread, Event
from bdaq53.analysis import analysis_utils as au
from basil.dut import Dut
import logging
from pyvisa.errors import VisaIOError
from serial import SerialException


class Env_Monitoring(object):
    def __init__(self, *args, **kwargs):
        self.logger = logging.getLogger("Env Monitoring")
        self.logger.setLevel(logging.INFO)
        file_handler = logging.FileHandler('sample.log')
        formatter = logging.Formatter('%(asctime)s:%(message)s')
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)
        self.logger.info("init Monitoring")
        self.configuration = self._load_testbench_cfg(os.path.join(os.path.dirname(__file__), 'interlock.yaml'))
        if self.configuration['bench']['general']['bdaq_testbech_yaml'] is not None:
            self.testbench_config = self._load_testbench_cfg(self.configuration['bench']['general']['bdaq_testbech_yaml'])
        else:
            self.testbench_config = self._load_testbench_cfg(os.path.join(os.path.join(os.path.dirname(__file__), '..'), 'testbench.yaml'))
        self.context = zmq.Context()
        # Main working directory
        if self.configuration['bench']['general']['output_directory'] is not None:
            self.working_dir = self.configuration['bench']['general']['output_directory']
        else:
            self.working_dir = os.path.join(os.path.dirname(__file__), 'Env_Data')
        self.t_file = os.path.join(self.working_dir, f'{time.strftime("%Y-%m-%d")}-environment_monitoring.h5')

        with tb.open_file(self.t_file, 'a') as infile:
            try:
                base_description = dict({'timestamp': tb.Float64Col(pos=0), 'timestamp_text': tb.StringCol(64, pos=1), 'NTC_temp': tb.Float64Col(pos=2),
                                        'T_1': tb.Float64Col(pos=3), 'T_2': tb.Float64Col(pos=4), 'Hum_1': tb.Float64Col(pos=5),
                                         'Hum_2': tb.Float64Col(pos=6), 'dew_1': tb.Float64Col(pos=7), 'dew_2': tb.Float64Col(pos=8),
                                         'dew_NTC': tb.Float64Col(pos=9), 'I_leak': tb.Float64Col(pos=10), 'Vin': tb.Float64Col(pos=11),
                                         'Iin': tb.Float64Col(pos=12)})
                root = infile.root
                infile.create_table(where=root, name='environment_monitoring', description=base_description)
            except tb.NodeError:
                pass
        for module_cfg_key, module_cfg in {k: v for k, v in self.testbench_config['bench']['modules'].items() if 'identifier' in v.keys()}.items():
            self.HV = module_cfg["powersupply"].get('hv_voltage', None)
            self.HV_current_limit = module_cfg["powersupply"].get('hv_current_limit', 1e-5)
            self.LV_current = module_cfg["powersupply"].get('lv_current', None)
            self.LV = module_cfg["powersupply"].get('lv_voltage', None)
            self.LV_channel = module_cfg["powersupply"].get('lv_channel', 2)
        self.maximum_module_temperature = self.configuration['bench']['general'].get('maximum_module_temperature', 35)
        self.HV_current_limit = self.configuration['bench']['general'].get('hv_current_limit', 1e-5)
        self.interlock = Event()
        self.message = None
        self.message_was_send = Event()
        self.devices_are_stopped = Event()
        self.context = zmq.Context()
        self.NTC = None
        self.dew_1 = None
        self.I_in = 0
        self.I_leak = 0
        self.Vin = None

    def init(self):
        self.T = time.time()
        # init of all devices
        if self.configuration['bench']['general']['devices_yaml'] is not None:
            self.devices_yaml = self.configuration['bench']['general']['devices_yaml']
        else:
            self.devices_yaml = os.path.join(os.path.dirname(__file__), 'devices.yaml')
        self.dut = Dut(os.path.join(os.path.dirname(os.path.realpath(__file__)), self.devices_yaml))
        time.sleep(0.1)  # sleep because of hmp4040
        self.dut.init()
        time.sleep(0.1)
        self.sensor1 = self.dut['Thermohygrometer2']
        self.sensor2 = self.dut['Thermohygrometer3']
        self.arduino = self.dut['NTCReadout']
        self.keith = self.dut["Sourcemeter"]
        # self.keith2 = self.dut["Multimeter"]
        self.hmp = self.dut["PowerSupply"]
        time.sleep(0.1)
        self.hmp.set_channel(int(self.LV_channel))
        time.sleep(0.1)
        if self.LV is not None:
            if self.LV_current is not None:
                self.hmp.set_voltage(float(self.LV))
                time.sleep(0.1)
                self.hmp.set_current(float(self.LV_current))
                time.sleep(0.1)
                self.hmp.on()
            else:
                self.hmp.off()
        else:
            self.hmp.off()
        time.sleep(0.1)
        if self.HV is not None:
            if int((self.hmp.get_status()[:-1])) == 1:
                self.keith.set_voltage(float(self.HV))
                self.keith.set_current_limit(float(self.HV_current_limit))
                self.keith.on()
            else:
                self.keith.off()
        else:
            self.keith.off()
        self.logger.info("All devices are initialized")

    def measure_NTC(self):
        R = self.arduino.get_res(0).get(0)
        A = 0.8676453371787721e-3
        B = 2.541035850140508e-4
        C = 1.868520310774293E-7
        return 1 / (A + B * np.log(R) + C * (np.log(R))**3) - 273.15

    def send_data(self, socket, interlock, data, name="CoolingData"):
        data_meta_data = dict(
            name=name,
            dtype=str(data.dtype),
            shape=data.shape,
            interlock=interlock,
            timestamp=time.time()
        )
        try:
            data_ser = utils.simple_enc(data, meta=data_meta_data)
            socket.send(data_ser, flags=zmq.NOBLOCK)
        except zmq.Again:
            pass

    def read_DCS(self):
        self.logger.info("Start DCS Readout")
        socket = self.context.socket(zmq.PUB)
        socket.bind('tcp://127.0.0.1:7000')
        time.sleep(2)  # wait for Arduino, TODO: could be optimised
        with tb.open_file(self.t_file, 'a') as infile:
            while True:
                self._read_DCS()
                if self.Vin is not None:
                    self._write_DCS(table=infile.root.environment_monitoring)
                self.send_data(socket=socket, interlock=self.interlock.is_set(), data=np.array([self.T_1, self.T_2, self.Hum_1, self.Hum_2, self.dew_1, self.dew_2, self.NTC, self.I_leak, self.Vin, self.I_in], dtype=np.float64))
                if self.interlock.is_set():
                    # Slack message
                    if not self.message_was_send.is_set():
                        self.send_slack_message(f'Date:{time.strftime("%Y-%m-%d")}' + "\n" + f'Time:{time.strftime("%H-%M-%S")}' + "\n"
                                                + "*Interlock was raised*" + "\n" + f"{self.message}")
                        self.message_was_send.set()

    def _read_DCS(self):
        # TODO get vacuum pressure
        self.NTC = self.measure_NTC()
        self.timestamp = time.time()
        self.timestamp_text = time.strftime("%Y-%m-%d %H:%M:%S")
        # self.Vin = 0 # float(self.keith2.get_voltage()) - self.I_in * 0.084 * (1 + 3.9e-3 * (self.NTC - 25))
        self.T_1 = float(self.sensor1.get_temperature())
        self.T_2 = float(self.sensor2.get_temperature())
        self.Hum_1 = float(self.sensor1.get_humidity())
        self.Hum_2 = float(self.sensor2.get_humidity())
        self.dew_1 = self.calc_dew_point(T=self.T_1, RH=self.Hum_1)
        self.dew_2 = self.calc_dew_point(T=self.T_2, RH=self.Hum_1)
        self.dew_ntc = self.calc_dew_point(T=self.NTC, RH=self.Hum_1)

    def _write_DCS(self, table):
        row = table.row
        row['NTC_temp'] = self.NTC
        row['timestamp'] = self.timestamp
        row['timestamp_text'] = self.timestamp_text
        row['T_1'] = self.T_1
        row['T_2'] = self.T_2
        row['Hum_1'] = self.Hum_1
        row['Hum_2'] = self.Hum_2
        row['dew_1'] = self.dew_1
        row['dew_2'] = self.dew_2
        row['dew_NTC'] = self.dew_ntc
        row['I_leak'] = self.I_leak
        row["Vin"] = self.Vin
        row["Iin"] = self.I_in
        row.append()
        table.flush()

    def stop_devices(self):
        time.sleep(0.1)
        if self.HV is not None:
            self._turn_off_HV()
            time.sleep(0.1)
        if self.LV is not None:
            self._turn_off_LV()
        self.devices_are_stopped.set()

    def _turn_off_LV(self, n=10):
        counter = 0
        while (float((self.hmp.get_status()[:-1])) != 0) and (counter < n):
            try:
                time.sleep(0.1)
                self.hmp.off()
                time.sleep(0.1)
                self.logger.info("LV is turned off")
            except (VisaIOError, SerialException):
                self.logger.info("VisaIOError of HMP4040")
                counter += 1
        if counter == n:
            self.logger.error("LV could not be turned off!")

    def _turn_off_HV(self, n=10):
        counter = 0
        while (int(self.keith.get_on()) != 0) and (counter < n):
            try:
                self.keith.off()
                time.sleep(0.1)
                self.logger.info("HV is turned off")
            except SerialException:
                counter += 1
                self.logger.info("SerialException of Sourcemeter")
                time.sleep(0.1)
        if counter == 10:
            self.logger.error("HV could not be turned off!")

    def _load_testbench_cfg(self, bench_config):
        ''' Load the bench config into the scan

            Parameters:
            ----------
            bench_config : str or dict
                    Testbench configuration (configuration as dict or its filename as string)
        '''
        conf = au.ConfigDict()
        try:
            with open(bench_config) as f:
                conf['bench'] = yaml.full_load(f)
        except TypeError:
            conf['bench'] = bench_config

        return conf

    def calc_dew_point(self, T, RH):
        if T < 0:
            T_n = 243.12
            m = 17.6
        else:
            T_n = 272.62
            m = 22.46
        if RH == 0:
            RH = 0.01
        a = np.log(RH / 100.0) + m * T / (T_n + T)
        return T_n * a / (m - a)

    def check_interlock(self):
        self.logger.info("Start Interlock")
        while True:
            self._check_LV_HV()
            if not self.interlock.is_set():
                self._check_temperatures()
            if not self.interlock.is_set():
                self._check_power_of_HV()
            elif self.interlock.is_set():
                self.stop_devices()
                while not self.devices_are_stopped.is_set():
                    time.sleep(1)

    def _check_power_of_HV(self):
        if (self.HV is not None) and (self.LV is not None):
            try:
                if (int(self.keith.get_on()) == 1) & (int((self.hmp.get_status()[:-1])) != 1):
                    self.interlock.set()
                    self.message = "HV is powered and LV not"
                    if not self.message_was_send.is_set():
                        self.logger.error(self.message)
            except (VisaIOError, SerialException) as e:
                self.logger.info(f"{e}")

    def _check_LV_HV(self):
        try:
            if self.LV is not None:
                self.I_in = float(self.hmp.get_current())
                time.sleep(0.05)
                if self.NTC is not None:
                    self.Vin = float(self.hmp.get_voltage()) - self.I_in * 0.275 * (1 + 3.9e-3 * (self.NTC - 25))
                if self.HV is not None:
                    if int(self.keith.get_on()) == 1:
                        time.sleep(0.05)
                        self.I_leak = float(self.keith.get_current().split(",")[1])
                        time.sleep(0.05)
                        if not self.interlock.is_set():
                            if self.I_leak <= -0.95 * float(self.HV_current_limit):
                                self.interlock.set()
                                self.message = "95% of HV current limit of " + "{:.4e}".format(self.I_leak) + " A  is reached"
                                if not self.message_was_send.is_set():
                                    self.logger.error(self.message)
                    else:
                        self.I_leak = 0.0
        except (VisaIOError, SerialException) as e:
            self.logger.info(f"{e}")

    def _check_temperatures(self):
        if (self.NTC is not None) & (self.dew_1 is not None):
            if self.NTC <= self.dew_1:
                self.interlock.set()
                self.message = "Dewpoint is to high" + "\n" + "T = {:.2f} ".format(self.NTC) + "C" + " and Dp={:.2f}".format(self.dew_1) + "C"
                if not self.message_was_send.is_set():
                    self.logger.error(self.message)
            if self.NTC >= self.maximum_module_temperature:
                self.interlock.set()
                self.message = "Module temperature is higher than 35C" + "\n" + "T = {:.2f}".format(self.NTC) + "C"
                if not self.message_was_send.is_set():
                    self.logger.error(self.message)

    def send_slack_message(self, message):
        payload = '{"text":"%s"}' % message
        response = requests.post("https://hooks.slack.com/services/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", data=payload)
        self.logger.info(response.text)

    def run(self):
        thread_1 = Thread(target=self.read_DCS)
        thread_1.start()
        thread_2 = Thread(target=self.check_interlock)
        thread_2.start()


if __name__ == '__main__':
    Monitor = Env_Monitoring()
    Monitor.init()
    try:
        Monitor.run()
    except KeyboardInterrupt:
        pass
