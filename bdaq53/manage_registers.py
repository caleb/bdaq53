#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import yaml
import logging

import gspread
from oauth2client.service_account import ServiceAccountCredentials


class RegisterExtraction():
    '''
    Generates local lookup file in YAML format from Google spreadsheet
    '''

    def __init__(self, chip_type='rd53a'):
        logging.root.handlers = []
        self.log = logging.getLogger('RegisterExtraction')
        logging.basicConfig(level=logging.INFO, format='%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s')
        if chip_type == 'rd53a':
            self.wsheet = 1
            self.val_offset = 1
        elif chip_type == 'ITkPixV1':
            self.wsheet = 8
            self.val_offset = 0
        elif chip_type == 'CROCv1':
            self.wsheet = 8  # 0
            self.val_offset = -1

    def open_worksheet(self, credfile, key, worksheet=0):
        '''
        In order for this to work, your Google accounts needs to have access to the spreadsheet
        and you need to activate the Google sheets API and create a credential file.
        For further information see https://developers.google.com/sheets/api/quickstart/python
        '''

        self.log.info('Try to connect to GoogleDocs using credentials from {0}'.format(credfile))
        scope = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(credfile, scope)
        gc = gspread.authorize(credentials)
        return gc.open_by_key(key).get_worksheet(self.wsheet)

    def _interpret_number(self, val, size=0):
        val = val.replace("_", "")
        val = val.replace("''", "'")
        if "'" in val:
            _, number = val.split("'")
            if number[0] == 'h':  # Hexadecimal
                out = int(number[1:], 16)
            elif number[0] == 'd':  # Decimal
                out = int(number[1:], 10)
            elif number[0] == 'b':  # Binary
                out = int(number[1:], 2)
            else:
                raise NotImplementedError
        elif val == '0':
            out = 0
        elif val == '1':
            out = 1
        elif val == 'n/a':
            out = 0
        elif val == 'all enabled':
            out = 2 ** size
        else:
            raise NotImplementedError
        return out

    def read_spreadsheet(self, sh):
        content = sh.get_all_values()

        registers = []
        for row in content:
            if row[3] == '' or row[3] == 'Register Name':
                continue
            try:
                name = row[3]
                address = int(row[2])
                size = int(row[5])
                default = self._interpret_number(row[8 + 1 - self.val_offset], size)
                description = row[7 + 1 - self.val_offset]
                mode = 1 if row[4] == 'R/W' else 0
                registers.append(
                    {'name': name, 'address': address, 'size': size, 'default': default, 'description': description,
                        'mode': mode, 'reset': 1})
            except Exception as e:
                self.log.error("Error handling register: " + name)
                break
                # raise Exception(e)

        return registers

    def dump_yaml(self, registers, filename='register_lookup.yaml', default_base=2, address_base=16):
        for reg in registers:
            if default_base == 2:
                reg['default'] = bin(reg['default'])
            elif default_base == 16:
                reg['default'] = hex(reg['default'])
            if address_base == 2:
                reg['address'] = bin(reg['address'])
            elif address_base == 16:
                reg['address'] = hex(reg['address'])

        data = {'registers': registers}

        with open(filename, 'w') as outfile:
            yaml.dump(data, outfile, default_flow_style=False)

        self.log.info('Register map successfully dumped!')
        self.log.debug(registers)


if __name__ == '__main__':
    '''
    Dump the spreadsheet to a lookup file
    '''
    re = RegisterExtraction(chip_type='CROCv1')
    sh = re.open_worksheet(credfile='creds.json', key='[Your API key]')
    registers = re.read_spreadsheet(sh)
    re.dump_yaml(registers, filename='chips/CROCv1_registers.yaml', default_base=2, address_base=16)
