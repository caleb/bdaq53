#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
from unittest import mock
import numpy as np

from bdaq53.chips import rd53a
from bdaq53.tests import utils
from copy import deepcopy


def update(self, force=False, broadcast=False):
    ''' Original chip.masks.update function used to cross check results
    '''
    if force and self.chip.bdaq.board_version == 'SIMULATION':
        return []

    self._find_changes()
    indata = self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0)  # Disable default config

    if force:
        write_mask = np.ones(self.dimensions, bool)
    else:   # Find out which pixels need to be updated
        write_mask = self.to_write
    to_write = np.column_stack((np.where(write_mask)))

    data = []

    if len(to_write) < 4000:   # Write only double pixels which need to be updated
        self.chip.registers['PIX_MODE'].set(0)
        indata += self.chip.registers['PIX_MODE'].get_write_command(0)    # Disable broadcast, auto-col, auto_row

        indata += self.chip.write_sync_01(write=False)
        last_coords = (-1, -1, -1, -1, -1, -1)
        written = []
        for (col, row) in to_write:
            (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, row)
            (region_col, region_row) = self.remap_to_registers(core_col, core_row, region, pixel_pair)

            # Speedup block
            if (core_col, core_row, region, pixel_pair) in written:  # This pixel pair has already been updated
                continue
            if col != last_coords[0]:  # Downshifting patterns always changes two pixels below each other
                indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
            if row != last_coords[1]:  # Sideshifting patterns always changes two pixels next to each other
                indata += self.chip.registers['REGION_ROW'].get_write_command(int(region_row, 2))
            last_coords = (col, row, core_col, core_row, region, pixel_pair)
            written.append((core_col, core_row, region, pixel_pair))

            # Write pixel pair value
            indata += self.chip.registers['PIX_PORTAL'].get_write_command(self.get_double_pixel_data(core_col, core_row, region, pixel_pair))

            if len(indata) > 4000:    # Write command to chip before it gets too long
                self.chip.write_command(indata)
                data.append(indata)
                indata = self.chip.write_sync_01(write=False)

        self.chip.write_command(indata)
        data.append(indata)
    else:   # Use auto-row
        self.chip.registers['PIX_MODE'].set(0b001000)
        indata += self.chip.registers['PIX_MODE'].get_write_command(0b001000)    # Disable broadcast, enable auto-row

        indata += self.chip.write_sync_01(write=False)
        for col in range(0, 400, 2):   # Loop over every second column, since two neighboring pixels are always written together
            # Set column address to next column pair
            (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, 0)
            (region_col, _) = self.remap_to_registers(core_col, core_row, region, pixel_pair)
            indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))

            # Set row to 0
            indata += self.chip.registers['REGION_ROW'].get_write_command(0)

            for row in range(192):  # Loop over every row, write double pixels
                (_, core_row, region, _, _) = self.remap_to_global(col, row)

                indata += self.chip.registers['PIX_PORTAL'].get_write_command(self.get_double_pixel_data(core_col, core_row, region, pixel_pair))

                if len(indata) > 4000:  # Write command to chip before it gets too long
                    self.chip.write_command(indata)
                    data.append(indata)
                    indata = self.chip.write_sync_01(write=False)

        self.chip.write_command(indata)
        data.append(indata)

    # Set this mask as last mask to be able to find changes in next update()
    for name, mask in self.items():
        self.was[name][:] = mask[:]

    return data


def assemble_double_pixels_data_aligned_old(chip, mask):
    ''' Assembles double pixel data as in chip.mask.update()

        Same word order as in chip.mask.update() for auto-row pixel address mode.
        Only pixel register values are assembled.
    '''
    double_pixel_data = []
    for col in range(mask.shape[0]):
        if col % 2:  # double pixel data is for two columns
            continue
        for row in range(mask.shape[1]):
            if mask[col, row] or mask[col + 1, row]:
                core_col, core_row, region, pixel_pair, _ = chip.masks.remap_to_global(col, row)
                double_pixel_data.append(chip.masks.get_double_pixel_data(core_col, core_row, region, pixel_pair))
    return np.array(double_pixel_data, dtype=np.uint16)


def assemble_double_pixels_data_old(chip, mask):
    ''' Assembles double pixel data as in chip.mask.update()

        Same word order as in chip.mask.update() for direct double pixel address mode.
        Only pixel register values are assembled.
    '''
    double_pixel_data = []
    written = []
    for col in range(mask.shape[0]):
        for row in range(mask.shape[1]):
            core_col, core_row, region, pixel_pair, _ = chip.masks.remap_to_global(col, row)
            if (core_col, core_row, region, pixel_pair) in written:
                continue
            if mask[col, row]:
                double_pixel_data.append(chip.masks.get_double_pixel_data(core_col, core_row, region, pixel_pair))
                written.append((core_col, core_row, region, pixel_pair))
    return np.array(double_pixel_data, dtype=np.uint16)


def random_mask(n_true, seed=None, shape=(400, 192)):
    ''' Return a bolean mask with n_true enabled elements at random positions '''
    if seed is not None:
        np.random.seed(seed)  # allow reproducible random values
    mask = np.zeros(shape, dtype=bool).ravel()
    indeces = np.random.choice(mask.shape[0], n_true, replace=False)
    mask[indeces] = True
    return mask.reshape(shape)


class TestRD53a(unittest.TestCase):

    def conf_pix_reg_random(self, chip, seed=None, update=True):
        ''' Configure chip with random register values'''
        if seed is not None:
            np.random.seed(seed)  # allow reproducible random values
        chip.masks['enable'][:] = np.random.randint(2, size=chip.masks['enable'].shape).astype(bool)
        chip.masks['injection'][:] = np.random.randint(2, size=chip.masks['enable'].shape).astype(bool)
        for fe, cols in rd53a.FLAVOR_COLS.items():
            if fe == 'SYNC':
                chip.masks['tdac'][cols[0]:cols[1], :] = 0
            if fe == 'LIN':
                size = chip.masks['tdac'][cols[0]:cols[1], :].shape
                tdac_max, tdac_min, _, _ = rd53a.get_tdac_range(fe)
                chip.masks['tdac'][cols[0]:cols[1], :] = np.random.randint(low=tdac_min, high=tdac_max, size=size)
                chip.masks['lin_gain_sel'][:] = np.random.randint(2, size=size).astype(bool)
            if fe == 'DIFF':
                tdac_min, tdac_max, _, _ = rd53a.get_tdac_range(fe)
                chip.masks['tdac'][cols[0]:cols[1], :] = np.random.randint(low=tdac_min, high=tdac_max, size=chip.masks['tdac'][cols[0]:cols[1], :].shape)

        chip.masks.apply_disable_mask()
        if update:
            chip.masks.update()

    def test_pixel_register_commands_auto_row(self):
        ''' Check that fast pixel register update command assembley with auto row equals the slow assembley. '''

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command'):
            chip = rd53a.RD53A(bdaq=mock.Mock())
            self.conf_pix_reg_random(chip)
            # Pixels to update, for > 10000 auto row feature is used
            mask = random_mask(400 * 192 // 2)

            double_pixel_data = rd53a.get_double_pixels_data(mask,
                                                             enable=chip.masks['enable'],
                                                             injection=chip.masks['injection'],
                                                             hitbus=chip.masks['hitbus'],
                                                             tdac=chip.masks['tdac'],
                                                             lin_gain_sel=chip.masks['lin_gain_sel'],
                                                             dc_aligned=True)  # auto row aligns words at even columns
            double_pixel_data_old = assemble_double_pixels_data_aligned_old(chip, mask)
            self.assertTrue(utils.nan_equal(double_pixel_data, double_pixel_data_old))

    def test_pixel_register_commands(self):
        ''' Check that fast pixel register update command assembley with direct pixel addressing equals the slow assembley. '''

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command'):
            chip = rd53a.RD53A(bdaq=mock.Mock())
            self.conf_pix_reg_random(chip)
            # Pixels to update, for < 10000 to use direct double pixel addressing
            mask = random_mask(5000)

            double_pixel_data = rd53a.get_double_pixels_data(mask,
                                                             enable=chip.masks['enable'],
                                                             injection=chip.masks['injection'],
                                                             hitbus=chip.masks['hitbus'],
                                                             tdac=chip.masks['tdac'],
                                                             lin_gain_sel=chip.masks['lin_gain_sel'],
                                                             dc_aligned=False)  # direct addressing is not aligned
            double_pixel_data_old = assemble_double_pixels_data_old(chip, mask)

            self.assertTrue(utils.nan_equal(double_pixel_data, double_pixel_data_old))

    def test_pixel_register_command_assembley_order(self):
        ''' Check that the pixel register update command assembley when aligning at DCs only changes the word order but not the words itself '''

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command'):
            chip = rd53a.RD53A(bdaq=mock.Mock())
            self.conf_pix_reg_random(chip)
            mask = random_mask(20000)

            double_pixel_data_aligned = rd53a.get_double_pixels_data(mask,
                                                                     enable=chip.masks['enable'],
                                                                     injection=chip.masks['injection'],
                                                                     hitbus=chip.masks['hitbus'],
                                                                     tdac=chip.masks['tdac'],
                                                                     lin_gain_sel=chip.masks['lin_gain_sel'],
                                                                     dc_aligned=True)  # aligns words at even columns

            double_pixel_data = rd53a.get_double_pixels_data(mask,
                                                             enable=chip.masks['enable'],
                                                             injection=chip.masks['injection'],
                                                             hitbus=chip.masks['hitbus'],
                                                             tdac=chip.masks['tdac'],
                                                             lin_gain_sel=chip.masks['lin_gain_sel'],
                                                             dc_aligned=False)  # no dc alignments

            # The order must be different
            self.assertFalse(utils.nan_equal(double_pixel_data_aligned, double_pixel_data))
            # Check that the words are equal, alothugh the order is different
            self.assertTrue(utils.nan_equal(np.sort(double_pixel_data_aligned), np.sort(double_pixel_data)))

    def test_update_pixel_registers(self):
        ''' Check the chip.update procedure '''

        cmds = []

        def store_cmd(cmd):
            cmds.append(cmd)

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
            # Test update with auto-row (> 4000 pixels are changed)
            chip = rd53a.RD53A(bdaq=mock.Mock())
            self.conf_pix_reg_random(chip, seed=0, update=False)
            chip.masks['enable'] = random_mask(20000)  # ensure more than 10000 changed pixels
            cmds = []

            update(chip.masks)
            cmds_old = deepcopy(cmds)

            chip = rd53a.RD53A(bdaq=mock.Mock())
            self.conf_pix_reg_random(chip, seed=0, update=False)
            chip.masks['enable'] = random_mask(20000)  # ensure more than 10000 changed pixels
            cmds = []
            chip.masks.update()
            cmds_new = deepcopy(cmds)

            self.assertListEqual(cmds_old, cmds_new)

            # Test update with direct double pixel writing
            chip = rd53a.RD53A(bdaq=mock.Mock())
            chip.masks['enable'] = random_mask(5000, seed=0)  # ensure less than 10000 changed pixels
            cmds = []

            update(chip.masks)
            cmds_old = deepcopy(cmds)

            chip = rd53a.RD53A(bdaq=mock.Mock())
            chip.masks['enable'] = random_mask(5000, seed=0)  # ensure more than 10000 changed pixels
            cmds = []
            chip.masks.update()
            cmds_new = deepcopy(cmds)

            self.assertListEqual(cmds_old, cmds_new)


if __name__ == '__main__':
    unittest.main()
