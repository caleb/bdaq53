`ifndef GEARBOX_4_TO_66__SV
`define GEARBOX_4_TO_66__SV

`timescale 1ns/1ps

module DeserializerGearbox4to66
  (input wire logic [3:0] Data4,
   input wire logic Rst_b,
   input wire logic Clk,

   output logic [65:0] Data66,
   output logic DataValid);
   
   logic 	shift2;
   logic [67:0] buffer_68_notmr;
   logic [65:0] data66_notmr;
   logic [4:0] 	counter;

   always_ff @(posedge Clk) begin
      if (Rst_b == 1'b0) begin
	 counter <= '0;
	 buffer_68_notmr <= '0;
	 data66_notmr <= '0;
	 shift2 <= '0;
	 DataValid <= 1'b0;
      end else begin
	 if ((counter == 5'd16) || ((shift2 == 1'b1) && (counter == 5'd15))) begin
	    counter <= '0;
	    shift2 <= ~shift2;
	 end else begin
	    counter <= counter + 1;
	 end

	 if ((counter == 5'd0)) begin
	    DataValid <= 1'b1;
	    if (shift2 == 1'b0) begin
	       data66_notmr <= {buffer_68_notmr[0 +: 64], buffer_68_notmr[67:66]};
	    end else begin
	       data66_notmr <= buffer_68_notmr[0 +: 66];
	    end
	 end else begin
	    DataValid <= 1'b0;
	 end

	 buffer_68_notmr[(counter*4) +: 4] <= Data4;
      end
   end // always_ff @ (posedge Clk)

   assign Data66 = data66_notmr;
   
endmodule // DeserializerGearbox4to66

`endif
