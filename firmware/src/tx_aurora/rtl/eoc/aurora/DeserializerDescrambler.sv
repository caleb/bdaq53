
`ifndef DESERIALIZER_DESCRAMBLER__SV   // include guard
`define DESERIALIZER_DESCRAMBLER__SV

`timescale 1 ns / 1 ps

module DeserializerDescrambler(
		 input wire [63:0] DataIn,
		 input wire [1:0] SyncBits,
		 input wire Ena,
		 input wire Clk,
		 input wire Rst_b,
		 output logic [65:0] DataOut
		 );

   logic [57:0] 		     scrambled_data_notmr, poly;
   logic [65:0] 		     unscrambled_data_notmr;
   
   logic [63:0] 		     outvec_notmr;
   
   integer 			     i;
   
   
   always_ff @(posedge Clk) begin
      if (Rst_b == 1'b0)
	scrambled_data_notmr <= '{default:1};
      else
	if (Ena) begin
		scrambled_data_notmr <= poly;
		unscrambled_data_notmr <= {outvec_notmr,SyncBits[0],SyncBits[1]};
	   end
   end

   always @(scrambled_data_notmr,DataIn)
     begin
             poly = scrambled_data_notmr;
             for (i=0;i<64;i=i+1)
             begin
                 outvec_notmr[i] = DataIn[i] ^ poly[38] ^ poly[57];
                 poly = {poly[56:0],DataIn[i]};
             end
     end
   
   assign DataOut = unscrambled_data_notmr;
   
endmodule : DeserializerDescrambler

`endif   // DESERIALIZER_DESCRAMBLER__SV

