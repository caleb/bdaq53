`ifndef DESERIALIZER_FIND_CB__SV
 `define DESERIALIZER_FIND_CB__SV

module DeserializerFindCB 
  #(
    parameter WWIDTH = 3
    )
   (
    input wire 		      Reset_b,
    input wire 		      Clk,
    input wire [65:0] 	      Block,
    input wire 		      Valid,

    output logic 	      Wrapped, 
    output logic 	      FoundCB,
    output logic [WWIDTH-1:0] PositionCB
    );

   logic [WWIDTH-1:0] 	      windowpos;
   logic 		      isCB;
   logic 		      int_foundCB;
   

   assign isCB = ((Block[65-:2] == 2'b10) && 
		  (Block[63-:8] == 8'h78) && 
		  (Block[55-:4] == 4'b0100));
   

   always_ff @(posedge Clk)
     begin
	if (Reset_b == 1'b0) begin
	   int_foundCB <= 1'b0;
	   PositionCB <= {WWIDTH{1'b0}};
	   windowpos <= {WWIDTH{1'b0}};
	   Wrapped <= 1'b0;
	   
	end else begin
	   if (Valid) begin
	      if (isCB | int_foundCB) begin
		 windowpos <= windowpos + 1;
		 PositionCB <= windowpos;
	      end
	      if (windowpos == {WWIDTH{1'b1}})
		Wrapped <= 1'b1;
	      if (isCB) begin
		 int_foundCB <= 1'b1;
	      end
	   end
	end
     end // always_ff @

//   assign FoundCB = isCB || int_foundCB;
   assign FoundCB = int_foundCB;
   
   
endmodule // DeserializeFindCB


`endif
