
`ifndef DESERIALIZER_SIM__SV
`define DESERIALIZER_SIM__SV

`timescale 1ns/1ps

module DeserializerSim (

   input wire SerIn,
   input wire Clk1280,
   output wire ClkSampling,

   output logic [3:0] Qa,
   output logic [3:0] Qb,
   output logic [3:0] Qc,
   output logic [3:0] Qd);

   bit 		      clk640;
   bit 		      clk320;
   bit 		      clk160;
   logic 	      bufIn, negBufIn;
   logic [1:0] 	      posSample;
   logic [1:0] 	      negSample;

   logic [3:0] 	      bufQa;
   logic [3:0] 	      bufQb;
   logic [3:0] 	      bufQc;
   logic [3:0] 	      bufQd;
   

   always_ff @(posedge Clk1280) begin
      clk640 <= ~clk640;
   end

   always_ff @(posedge clk640) begin
      clk320 <= ~clk320;
   end

   always_ff @(posedge clk320) begin
      clk160 <= ~clk160;
   end

   always_ff @(posedge Clk1280)
     bufIn <= SerIn;

   always_ff @(negedge Clk1280)
     negBufIn <= SerIn;

   always_ff @(posedge Clk1280) begin
      posSample <= {posSample[0], bufIn};
      negSample <= {negSample[0], negBufIn};
   end

   always_ff @(posedge clk640) begin
      bufQa <= {bufQa[2:0], posSample[0]};
      bufQc <= {bufQc[2:0], posSample[1]};
      bufQb <= {bufQb[2:0], negSample[0]};
      bufQd <= {bufQd[2:0], negSample[1]};
   end

   always_ff @(posedge clk160) begin
      Qa <= bufQa;
      Qb <= bufQb;
      Qc <= bufQc;
      Qd <= bufQd;      
   end

   assign ClkSampling = clk160;
   
endmodule // DeserializerSim

`endif //  `ifndef DESERIALIZER_SIM__SV
