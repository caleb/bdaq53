# LocalDB interface for BDAQ53
# To be used with the localDB Docker stack provided by BDAQ53.
# Also setups the database that is usually not needed when using the CLI

import os
import time
import socket
import math
import logging
import yaml
import json
import hashlib

import pymongo
import gridfs

import numpy as np
import tables as tb
from bson import ObjectId
from datetime import datetime

from bdaq53.system import logger
from bdaq53.system.scan_base import fill_dict_from_conf_table

from bdaq53.analysis import analysis_utils as au
from bdaq53.manage_configuration import convert_config_bdaq_to_yarr

bdaq53_localdb_path = os.path.dirname(__file__)

LOCALDB_HOST = "127.0.0.1"
LOCALDB_PORT = 27017  # default mongodb port
DB_VERSION = 1.01


class LocalDBError(Exception):
    pass


class DBNotInitializedError(Exception):
    pass


class UploadDataError(Exception):
    pass


class ScanExistsError(Exception):
    pass


class DBReset(Exception):
    pass


class LocalDBinterface(object):
    def __init__(self, host, port, debug=False, dummy=False, _reset_db=None):
        self.debug = debug
        self.dummy = dummy

        self.db_initialized = False
        self.log = logger.setup_derived_logger("LocalDBinterface", level=logging.DEBUG if debug else logging.INFO)
        self._setup_connection(host, port, _reset_db=_reset_db)

        if self.dummy:
            self.log.warning("Running in dummy mode! Nothing will be written do localDB.")

    def _setup_connection(self, host, port, _reset_db=None):
        self.host = host
        self.port = port
        client = pymongo.MongoClient(host=host, port=port, serverSelectionTimeoutMS=3000)
        self.log.debug(client.server_info())
        self.log.success("Connected to localDB at {0}:{1}".format(host, port))

        self.DB_localdb = client["localdb"]
        self.DB_localdbtools = client["localdbtools"]
        self.FS_localdb = gridfs.GridFS(self.DB_localdb)

        # Reset all data if _reset_db flag
        if _reset_db is not None:
            self._reset_database(_reset_db)

        # Check if DB is initialized
        dbs = client.list_database_names()
        self.log.debug("Available databases:\n{}".format(dbs))
        if "localdb" not in dbs:
            self.log.error("DB 'localdb' not found!")
            # Raise exception, since this requires user intervention
            raise LocalDBError("Please download institution and module types info from the ITkPD via the viewer application GUI (Admin page)!")

        collections = self.DB_localdb.list_collection_names()
        self.log.debug("Available collections in DB 'localdb':\n{}".format(collections))
        tools_collections = self.DB_localdbtools.list_collection_names()
        self.log.debug("Available collections in DB 'localdbtools':\n{}".format(tools_collections))

        if not ("institution" in collections and "pd.institution" in collections):
            self.log.error("Collection 'institutions' not found!")
            # Raise exception, since this requires user intervention
            raise LocalDBError("Please download institution and module types info from the ITkPD via the viewer application GUI (Admin page)!")

        # Check for everything else except for fs.chunks
        # if 'chip' not in collections or 'component' not in collections or 'componentTestRun' not in collections or 'config' not in collections or 'fs.files' not in collections or 'testRun' not in collections or 'user' not in collections:
        if "component" not in collections or "componentTestRun" not in collections or "fs.files" not in collections or "testRun" not in collections:
            self.log.warning("localDB instance is not yet initialized!")
        else:
            self.db_initialized = True

    def init(self, user=None, institution=None):
        """
            Initialize localDB if necessary.
            Define self.institution and self.user variables from provided parameters or config file created by 'setup_localdb.sh'.
        """

        # If DB not initialized, create necessary collections and indexes
        if not self.db_initialized:
            raise DBNotInitializedError('Not initialized. Run "bdaq_localdb --init" ')

        # Load local configuration
        try:
            with open(os.path.join(bdaq53_localdb_path, ".localdb.yaml")) as c:
                config = yaml.safe_load(c)
        except FileNotFoundError:
            config = None

        # Define self.institution and self.user
        if institution is not None:
            self.institution = self._get_institution(institution)
        elif config is not None:
            self.institution = self._get_institution(config["user"]["institution"])
        else:
            raise RuntimeError("No institution defined!")
        self.log.debug("Selected institution:\n{}".format(self.institution))

        if user is not None:
            self.user = self._get_user(user)
        elif config is not None:
            self.user = self._get_user(config["user"]["name"])
        else:
            raise RuntimeError("No user defined!")
        self.log.debug("Selected user:\n{}".format(self.user))

    def _reset_database(self, mode):
        """ Drop all non-default information from the database. Recommended only for debugging. """

        self.log.critical("CAREFUL, this will drop all information from localDB in 3s...")
        self.log.critical("3...")
        time.sleep(1)
        self.log.critical("2...")
        time.sleep(1)
        self.log.critical("1...")
        time.sleep(1)

        if mode in ["soft", "data"]:
            self.DB_localdb["fs.files"].drop()
            self.DB_localdb["fs.chunks"].drop()
            self.DB_localdb["chip"].delete_one({})
            self.DB_localdb["config"].delete_one({})
            self.DB_localdb["environment"].delete_one({})
            self.DB_localdb["componentTestRun"].delete_one({})
            self.DB_localdb["testRun"].delete_one({})
            self.DB_localdbtools["viewer.query"].delete_one({})
            self._write_index(self.DB_localdbtools["viewer.query"], {"runId": "config", "timeStamp": datetime.utcnow()})
        elif mode in ["hard", "all"]:
            client = pymongo.MongoClient(host=self.host, port=self.port, serverSelectionTimeoutMS=3000)
            client.drop_database("localdb")

            self.DB_localdbtools["QC.module.types"].drop()
            self.DB_localdbtools["QC.status"].drop()
            self.DB_localdbtools["viewer.query"].delete_one({})

            self._write_index(self.DB_localdbtools["viewer.query"], {"runId": "config", "timeStamp": datetime.utcnow()})

        raise DBReset("LocalDB was reset to default values!")

    def _get_institution(self, code):
        """ Return institution object from localDB for given institution code """

        if "institution" not in self.DB_localdb.list_collection_names():
            # No institution collection defined -> Database is not yet initialized!
            raise DBNotInitializedError("No institutions defined in the localDB yet. Make sure to download the list of institutions from the PDB via the GUI.")

        for institution in self.DB_localdb.institution.find():  # List all existing institutions
            try:
                if institution["code"] == code:
                    self.log.debug("Institution {} found in localDB.".format(code))
                    return institution
            except KeyError:  # There is no code if unknown institution uploaded through YARR?!
                pass
        else:  # Institution code does not exist
            raise LocalDBError("Institution not found: {}".format(code))

    def _get_user(self, name):
        """
            Return user object from localDB for given user name.
            Create a fresh user object if user not yet defined in localDB.
        """

        fresh_user = {"userName": name, "USER": name, "institution": self.institution["code"], "description": "default", "HOSTNAME": socket.getfqdn(), "userType": "readWrite"}

        userlist = list(self.DB_localdb.user.find())
        self.log.debug("Available users in DB 'localdb':\n{}".format(userlist))
        for user in userlist:  # List all existing users
            # Check if user name from local config file already exists...
            if user["userName"] == name or user["USER"] == name:
                self.log.debug("User {} found in localDB.".format(name))
                return user
        else:  # Create new user
            self.log.warning("Username '{}' not found in localDB. Creating new user...".format(name))
            oid = self._write_index(self.DB_localdb.user, fresh_user)
            return self.DB_localdb.user.find_one({"_id": ObjectId(oid)})

    def _write_index(self, collection, data):
        if self.dummy:
            return "123"

        def _update_sys(data):
            if "sys" not in data.keys():
                data["sys"] = {"cts": datetime.utcnow(), "mts": datetime.utcnow(), "rev": 0}
            else:
                data["sys"]["mts"] = datetime.utcnow()
                data["sys"]["rev"] += 1
            return data

        data = _update_sys(data)
        if "dbVersion" not in data.keys():
            data["dbVersion"] = DB_VERSION

        if "_id" in data.keys():
            result = collection.replace_one({"_id": data["_id"]}, data)
            if result.modified_count == 1:
                return data["_id"]
            else:
                raise
        else:
            return str(collection.insert_one(data).inserted_id)

    def _put_json_as_file(self, data, filename):
        if self.dummy:
            return "123"

        binary = json.dumps(data, indent=4).encode("utf-8")
        sha = hashlib.sha256(binary).hexdigest()
        return str(self.FS_localdb.put(binary, filename=filename, dbVersion=DB_VERSION, hash=sha))

    def _put_string_as_file(self, data, filename):
        if self.dummy:
            return "123"

        binary = data.encode("utf-8")
        sha = hashlib.sha256(binary).hexdigest()
        return str(self.FS_localdb.put(binary, filename=filename, dbVersion=DB_VERSION, hash=sha))

    def _read_components(self, node, is_module_data):
        ''' Read component information (module and chip)
        '''
        components = {"module": {}}
        module_node_data = fill_dict_from_conf_table(node.module)
        components["module"].update({"name": module_node_data["identifier"]})  # module name/identifier (has to be ATLAS S/N)
        components["module"].update({'chips': {}})
        if is_module_data:
            for j in node.chips:
                chip_settings = fill_dict_from_conf_table(j.settings)
                components["module"]["chips"].update({chip_settings['name']: {"name": self._convert_chip_sn(chip_settings['chip_sn']), "chip_sn": chip_settings['chip_sn'], "chip_id": chip_settings['chip_id'], "chip_type": chip_settings['chip_type'].upper()}})
            components["module"].update({"chip_type": chip_settings['chip_type'].upper()})
        else:
            chip_settings = fill_dict_from_conf_table(node.settings)
            components["module"]["chips"].update({chip_settings['name']: {"name": self._convert_chip_sn(chip_settings['chip_sn']), "chip_sn": chip_settings['chip_sn'], "chip_id": chip_settings['chip_id'], "chip_type": chip_settings['chip_type'].upper()}})
            components["module"].update({"chip_type": chip_settings['chip_type'].upper()})
        return components

    def _convert_chip_sn(self, chip_sn):
        ''' Converts chip S/N (0x....) to ATLAS S/N (20PGFC).
        '''
        return '20UPGFC{0:07d}'.format(int(chip_sn, 16))

    def _check_module_component(self, components):
        query = {"serialNumber": components["module"]['name'],
                 "componentType": "module",
                 "chipType": components["module"]['chip_type'],
                 "dbVersion": DB_VERSION}
        this_cmp = self.DB_localdb.component.find_one(query)
        oid_mo_component = None
        if this_cmp:  # component exists already
            self.log.debug("Found component (module) already in DB")
            oid_mo_component = str(this_cmp['_id'])
        return oid_mo_component

    def _check_chip(self, component):
        query = {"name": component["name"],
                 # "chipId": component["chip_id"],  # chip ID is not set in production data base
                 "chipType": component["chip_type"],
                 "dbVersion": DB_VERSION}

        this_cmp = self.DB_localdb.chip.find_one(query)
        oid_chip = None
        if this_cmp:  # component exists already
            self.log.debug("Found chip already in DB")
            oid_chip = str(this_cmp['_id'])
        return oid_chip

    def _check_chip_component(self, component):
        query = {"serialNumber": component["name"],
                 "componentType": "front-end_chip",
                 "chipType": component['chip_type'],
                 "dbVersion": DB_VERSION}
        this_cmp = self.DB_localdb.component.find_one(query)
        ch_oid_component = None
        if this_cmp:  # component exists already
            self.log.debug("Found component (chip) already in DB")
            ch_oid_component = str(this_cmp['_id'])
        return ch_oid_component

    def _check_child_parent(self, oid_mo_component, ch_oid_component):
        query = {"parent": oid_mo_component,
                 "child": ch_oid_component,
                 "status": "active",
                 "dbVersion": DB_VERSION}
        this_cmp = self.DB_localdb.childParentRelation.find_one(query)
        oid_childParentRelation = None
        if this_cmp:  # component exists already
            self.log.debug("Found childParentRelation already in DB")
            oid_childParentRelation = str(this_cmp['_id'])
        return oid_childParentRelation

    def _register_DCS_data(self, dcs_data):
        dcs_keys = ['NTC_temp', 'Vin']  # has to match with names in environmental data table from .h5 file
        run_data = {}
        for k in dcs_keys:
            run_data.update({k: [{"data": [{}], "description": k, "mode": "iv", "setting": 0, "num": 0}]})

        run_data = self._read_dcs_data(dcs_data, dcs_keys, run_data)
        self.log.debug("Environment data:\n{}".format(run_data))

        # with open("debug_DCS.json", "w") as f:
        #     json.dump(run_data, f, indent=4)

        oid_environment_data = self._write_index(self.DB_localdb.environment, run_data)
        return oid_environment_data

    def _read_dcs_data(self, dcs_data, dcs_keys, run_data):
        data = []
        for _ in dcs_keys:
            data.append([])
        for i, k in enumerate(dcs_keys):
            for dat, ts in zip(dcs_data[k], dcs_data['timestamp']):
                data[i].append({'date': datetime.fromtimestamp(ts), 'value': dat})
        for i, k in enumerate(dcs_keys):
            run_data[k][0]['data'] = data[i]
        return run_data

    def upload_data(self, in_file):
        """
            Upload a scan to the localDB based on _interpreted.h5 file.

            Parameters:
            ----------
            in_file : str / path-like
                Path to the '_interpreted.h5' file of the scan to upload
        """

        implemented_scans = ["digital_scan", "analog_scan", "threshold_scan", "global_threshold_tuning", "local_threshold_tuning"]

        data = {}

        def _load_chip_config(node):
            chip_config = au.ConfigDict()
            chip_config["settings"] = fill_dict_from_conf_table(node.settings)
            chip_config["calibration"] = fill_dict_from_conf_table(node.calibration)
            chip_config["trim"] = fill_dict_from_conf_table(node.trim)
            chip_config["registers"] = fill_dict_from_conf_table(node.registers)
            chip_config["module"] = fill_dict_from_conf_table(node.module)
            chip_config["use_pixel"] = node.use_pixel[:]
            chip_config["masks"] = {}
            for n in node.masks:
                chip_config["masks"][n.name] = n[:]
            return chip_config

        def _load_chip_config_from_module(node, chip_node):
            chip_config = au.ConfigDict()
            chip_config["settings"] = fill_dict_from_conf_table(chip_node.settings)
            chip_config["calibration"] = fill_dict_from_conf_table(chip_node.calibration)
            chip_config["trim"] = fill_dict_from_conf_table(chip_node.trim)
            chip_config["registers"] = fill_dict_from_conf_table(chip_node.registers)
            chip_config["module"] = fill_dict_from_conf_table(node.module)
            chip_config["use_pixel"] = node.use_pixel[:]
            chip_config["masks"] = {}
            for n in node.masks:
                chip_config["masks"][n.name] = n[:]
            return chip_config

        def _upload_scan_data(is_module_data, chip_component, ch_oid_component, ch_oid):
            def _convert_to_e(dac, use_offset=True):
                if use_offset:
                    e = dac * chip_config["calibration"]["e_conversion_slope"] + chip_config["calibration"]["e_conversion_offset"]
                    de = math.sqrt((dac * chip_config["calibration"]["e_conversion_slope_error"]) ** 2 + chip_config["calibration"]["e_conversion_offset_error"] ** 2)
                else:
                    e = dac * chip_config["calibration"]["e_conversion_slope"]
                    de = dac * chip_config["calibration"]["e_conversion_slope_error"]
                return e, de

            chip_sn = chip_component['chip_sn']
            actual_chip_id = chip_sn_to_chip_id[chip_sn]
            with tb.open_file(in_file, "r") as f:
                if is_module_data:
                    chip_node = f.get_node('/configuration_in/module/chips/%s' % chip_sn)
                    chip_config_in = _load_chip_config_from_module(node=f.root.configuration_in.module, chip_node=chip_node)
                else:
                    chip_config_in = _load_chip_config(node=f.root.configuration_in.chip)

            # Read configuration objects from interpreted h5 file
            bench_config, scan_config = au.ConfigDict(), au.ConfigDict()
            with tb.open_file(in_file, "r") as f:
                if is_module_data:  # configuration_out does not exist in module files
                    configuration_out = f.root.configuration_in
                else:
                    configuration_out = f.root.configuration_out
                for node in configuration_out.bench._f_list_nodes():
                    bench_config[node.name] = fill_dict_from_conf_table(node)
                for node in configuration_out.scan._f_list_nodes():
                    if node._v_name in ['enable', 'chips']:  # skip chips and enable node, not needed here
                        continue
                    scan_config[node.name] = fill_dict_from_conf_table(node)

                # Check which chip type
                if scan_config["run_config"]["chip_type"].upper() == 'RD53A':  # RD53A
                    n_cols, n_rows = 400, 192
                else:  # ITkPix
                    n_cols, n_rows = 400, 384
                self.log.debug('Chip type: {0}, cols/rows: {1}/{2}'.format(scan_config["run_config"]["chip_type"].upper(), n_cols, n_rows))

                # Check if data is from single chip or module
                if is_module_data:
                    chip_config = chip_config_in.copy()  # configuration_out does not exist for module file
                else:
                    chip_config = _load_chip_config(node=f.root.configuration_out.chip)

                chip_config["settings"]["chip_sn"] = self._convert_chip_sn(chip_config["settings"]["chip_sn"])  # convert chip sn to ATLAS S/N

                # Read scan data according to scan_id
                run_name = scan_config["run_config"]["run_name"]
                scan_id = scan_config["run_config"]["scan_id"]
                timestamp = "_".join(scan_config["run_config"]["run_name"].split("_")[:2])

                if scan_id not in implemented_scans:
                    raise NotImplementedError("LocalDB upload for {} is not yet implemented!".format(scan_id))

                # Check if scan_id is already in DB
                run_data = self.DB_localdb.testRun.find_one({"exec": run_name})
                if run_data is not None:
                    self.log.info("testRun {} already exists in localDB! Adding this scan to the existing testRun".format(run_name))
                    testRun = str(run_data["_id"])

                    component_run_data = self.DB_localdb.componentTestRun.find_one({"testRun": testRun, "name": chip_config["settings"]["chip_sn"]})
                    if component_run_data is not None:
                        self.log.error("componentTestRun {0} for chip {1} already exists in localDB!".format(run_name, chip_config["settings"]["chip_sn"]))
                        if not self.dummy:
                            raise ScanExistsError("componentTestRun {0} for chip {1} already exists in localDB!".format(run_name, chip_config["settings"]["chip_sn"]))
                else:
                    testRun = None

                self.log.info("Preparing upload for {}...".format(run_name))

                # Basic data for all scans.
                if is_module_data:
                    chip_id_map = f.root.configuration_in.module.chip_id[:]
                else:
                    chip_id_map = np.full(shape=(n_cols, n_rows), fill_value=actual_chip_id)
                chip_id_slice = [slice(np.argwhere(chip_id_map == actual_chip_id)[0, 0], np.argwhere(chip_id_map == actual_chip_id)[-1, 0] + 1),
                                 slice(np.argwhere(chip_id_map == actual_chip_id)[0, 1], np.argwhere(chip_id_map == actual_chip_id)[-1, 1] + 1)]
                if scan_id in ["digital_scan", "analog_scan", "threshold_scan"]:
                    HistRelBCID = f.root.HistRelBCID[:][chip_id_slice[0], chip_id_slice[1]]
                    HistOcc = f.root.HistOcc[:][chip_id_slice[0], chip_id_slice[1]]
                    # HistTot = f.root.HistTot[:]
                if scan_id in ["threshold_scan"]:
                    ThresholdMap = f.root.ThresholdMap[chip_id_slice[0], chip_id_slice[1]]
                    NoiseMap = f.root.NoiseMap[chip_id_slice[0], chip_id_slice[1]]
                    Chi2Map = f.root.Chi2Map[chip_id_slice[0], chip_id_slice[1]]

                # Masks selection.
                for mask_name in chip_config_in['masks'].keys():
                    chip_config_in['masks'][mask_name] = chip_config_in['masks'][mask_name][chip_id_slice[0], chip_id_slice[1]]

            # Prepare and upload files -> localdb.fs.files, localdb.fs.chunks
            if scan_id in ["digital_scan", "analog_scan", "threshold_scan"]:
                # Enable mask map
                enable_mask = chip_config["use_pixel"][chip_id_slice[0], chip_id_slice[1]]
                enable_mask[: scan_config["scan_config"]["start_column"], :] = False
                enable_mask[scan_config["scan_config"]["stop_column"] :, :] = False
                enable_mask[:, : scan_config["scan_config"]["start_row"]] = False
                enable_mask[:, scan_config["scan_config"]["stop_row"] :] = False

                enmask = {"Data": enable_mask.astype(float).tolist(), "Name": "EnMask", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0, "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5}, "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5}, "z": {"AxisTitle": "Enable"}}
                oid_fs_EnMask = self._put_json_as_file(enmask, filename="EnMask.json")
                if self.debug:
                    with open("debug_EnMask.json", "w") as f:
                        json.dump(enmask, f, indent=4)

                # Occupancy map
                occ_data = HistOcc.sum(axis=2).astype(float).tolist()
                occ = {"Data": occ_data, "Name": "OccupancyMap", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0, "x": {"AxisTitle": "Column", "Bins": n_cols, "High": n_cols + 0.5, "Low": 0.5}, "y": {"AxisTitle": "Row", "Bins": n_rows, "High": n_rows + 0.5, "Low": 0.5}, "z": {"AxisTitle": "Hits"}}
                oid_fs_OccupancyMap = self._put_json_as_file(occ, filename="OccupancyMap.json")
                if self.debug:
                    with open("debug_OccupancyMap.json", "w") as f:
                        json.dump(occ, f, indent=4)

                # Relative BCID histogram
                bcid = {"Data": HistRelBCID.sum(axis=(0, 1, 2)).astype(float).tolist(), "Name": "L1Dist", "Overflow": 15.0, "Type": "Histo1d", "Underflow": 0.0, "x": {"AxisTitle": "L1Id", "Bins": 32, "High": 31.5, "Low": -0.5}, "y": {"AxisTitle": "Hits"}, "z": {"AxisTitle": "z"}}
                oid_fs_L1Dist = self._put_json_as_file(bcid, filename="L1Dist.json")
                if self.debug:
                    with open("debug_L1Dist.json", "w") as f:
                        json.dump(bcid, f, indent=4)

            if scan_id in ["threshold_scan"]:
                # Threshold distribution
                plot_range = [v - scan_config["scan_config"]["VCAL_MED"] for v in range(scan_config["scan_config"]["VCAL_HIGH_start"], scan_config["scan_config"]["VCAL_HIGH_stop"] + 1, scan_config["scan_config"]["VCAL_HIGH_step"])]
                hist, bins = np.histogram(np.ravel(ThresholdMap[enable_mask]), bins=plot_range)

                plot_range = [_convert_to_e(v)[0] for v in plot_range]

                contents = "Histo1d\nThresholdDist-0\nThreshold [e]\nNumber of Pixels\nz\n{nbins} {lower_limit} {upper_limit}\n2 16\n{data}".format(nbins=len(bins) - 1, lower_limit=plot_range[0], upper_limit=plot_range[-1], data=" ".join(hist.astype(str).tolist()))

                oid_fs_ThresholdDist_0 = self._put_string_as_file(contents, filename="ThresholdDist-0.dat")
                if self.debug:
                    with open("debug_ThresholdDist-0.dat", "w") as f:
                        f.write(contents)

                # Threshold map
                contents = "Histo2d\nThresholdMap-0\nColumn\nRow\nThreshold [e]\n{0} 0.5 {1}\n{2} 0.5 {3}\n0 0\n".format(n_cols, n_cols + 0.5, n_rows, n_rows + 0.5)
                for row in range(n_rows):
                    for col in range(n_cols):
                        contents += str(_convert_to_e(ThresholdMap[col, row])[0]) + " "
                    contents += "\n"

                oid_fs_ThresholdMap_0 = self._put_string_as_file(contents, filename="ThresholdMap-0.dat")
                if self.debug:
                    with open("debug_ThresholdMap-0.dat", "w") as f:
                        f.write(contents)

                # Noise distribution
                data = NoiseMap[enable_mask]
                diff = np.amax(data) - np.amin(data)
                plot_range = np.arange(np.amin(data), np.amax(data) + diff / 100.0, diff / 100.0)
                hist, bins = np.histogram(np.ravel(data), bins=plot_range)
                plot_range = [_convert_to_e(v, use_offset=False)[0] for v in plot_range]

                contents = "Histo1d\nNoiseDist-0\nNoise [e]\nNumber of Pixels\nz\n{nbins} {lower_limit} {upper_limit}\n2 16\n{data}".format(nbins=len(bins) - 1, lower_limit=plot_range[0], upper_limit=plot_range[-1], data=" ".join(hist.astype(str).tolist()))

                oid_fs_NoiseDist_0 = self._put_string_as_file(contents, filename="NoiseDist-0.dat")
                if self.debug:
                    with open("debug_NoiseDist-0.dat", "w") as f:
                        f.write(contents)

                # Noise map
                contents = "Histo2d\nNoiseMap-0\nColumn\nRow\nNoise [e]\n{0} 0.5 {1}\n{2} 0.5 {3}\n0 0\n".format(n_cols, n_cols + 0.5, n_rows, n_rows + 0.5)
                for row in range(n_rows):
                    for col in range(n_cols):
                        contents += str(_convert_to_e(NoiseMap[col, row], use_offset=False)[0]) + " "
                    contents += "\n"

                oid_fs_NoiseMap_0 = self._put_string_as_file(contents, filename="NoiseMap-0.dat")
                if self.debug:
                    with open("debug_NoiseMap-0.dat", "w") as f:
                        f.write(contents)

                # Chi2 distribution
                data = Chi2Map[enable_mask]
                diff = np.amax(data) - np.amin(data)
                plot_range = np.arange(np.amin(data), np.amax(data) + diff / 100.0, diff / 100.0)
                hist, bins = np.histogram(np.ravel(data), bins=plot_range)

                contents = "Histo1d\nChi2Dist-0\nFit Chi2 / ndf\nNumber of Pixels\nz\n{nbins} {lower_limit} {upper_limit}\n2 16\n{data}".format(nbins=len(bins) - 1, lower_limit=plot_range[0], upper_limit=plot_range[-1], data=" ".join(hist.astype(str).tolist()))

                oid_fs_Chi2Dist_0 = self._put_string_as_file(contents, filename="Chi2Dist-0.dat")
                if self.debug:
                    with open("debug_Chi2Dist-0.dat", "w") as f:
                        f.write(contents)

                # Chi2 map
                contents = "Histo2d\nChi2Map-0\nColumn\nRow\nFit Chi2/ndf\n{0} 0.5 {1}\n{2} 0.5 {3}\n0 0\n".format(n_cols, n_cols + 0.5, n_rows, n_rows + 0.5)
                for row in range(n_rows):
                    for col in range(n_cols):
                        contents += str(Chi2Map[col, row]) + " "
                    contents += "\n"

                oid_fs_Chi2Map_0 = self._put_string_as_file(contents, filename="Chi2Map-0.dat")
                if self.debug:
                    with open("debug_Chi2Map-0.dat", "w") as f:
                        f.write(contents)

                # Scurve plot
                plot_range = [v - scan_config["scan_config"]["VCAL_MED"] for v in range(scan_config["scan_config"]["VCAL_HIGH_start"], scan_config["scan_config"]["VCAL_HIGH_stop"] + 1, scan_config["scan_config"]["VCAL_HIGH_step"])]

                data = []
                for i in range(len(plot_range) - 1):
                    scan_param_data = [v[i] for v in HistOcc[enable_mask]]
                    hist, bins = np.histogram(scan_param_data, range(101))
                    data.append(hist.tolist())

                scurve_0_json = {"Data": data, "Name": "sCurve-0", "Type": "Histo2d", "Underflow": 0.0, "Overflow": 0.0, "x": {"AxisTitle": "Vcal", "Bins": len(plot_range) - 1, "High": plot_range[-1] - 0.5, "Low": plot_range[0] - 0.5}, "y": {"AxisTitle": "Occupancy", "Bins": 100, "High": 100.5, "Low": 0.5}, "z": {"AxisTitle": "Number of pixels"}}

                oid_fs_sCurve_0 = self._put_json_as_file(scurve_0_json, filename="sCurve-0.json")
                if self.debug:
                    with open("debug_sCurve-0.json", "w") as f:
                        json.dump(scurve_0_json, f, indent=4)

            # Prepare config data -> localdb.config and config files -> localdb.fs.files, localdb.fs.chunks

            # ctrlCfg.json
            # This file contains no relevant information for BDAQ53. Use hardcoded example.
            ctrlCfg_json = {"ctrlCfg": {"cfg": {"cmdPeriod": 6.250000073038109e-09, "idle": {"word": 1768515945}, "pulse": {"interval": 500, "word": 1549575846}, "rxPolarity": 0, "specNum": 0, "spiConfig": 541200, "sync": {"interval": 32, "word": 2172551550}, "trigConfig": None, "txPolarity": 0}, "type": "spec"}}
            oid_fs_ctrlCfg = self._put_json_as_file(ctrlCfg_json, "ctrlCfg.json")
            config_ctrlCfg = {"filename": "ctrlCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "ctrlCfg", "format": "fs.files", "data_id": oid_fs_ctrlCfg}
            oid_ctrlCfg = self._write_index(self.DB_localdb.config, config_ctrlCfg)
            if self.debug:
                with open("debug_ctrlCfg.json", "w") as f:
                    json.dump(ctrlCfg_json, f, indent=4)

            # dbCfg.json
            # Use mostly hardcoded info for now.
            dbCfg_json = {"QC": False, "auth": "default", "dbName": "localdb", "environment": ["vddd_voltage"], "hostIp": self.host, "hostPort": self.port, "ssl": {"CAFile": "null", "PEMKeyFile": "null", "enabled": False}, "tls": {"CAFile": "null", "CertificateKeyFile": "null", "enabled": False}}
            oid_fs_dbCfg = self._put_json_as_file(dbCfg_json, "dbCfg.json")
            config_dbCfg = {"filename": "dbCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "dbCfg", "format": "fs.files", "data_id": oid_fs_dbCfg}
            oid_dbCfg = self._write_index(self.DB_localdb.config, config_dbCfg)
            if self.debug:
                with open("debug_dbCfg.json", "w") as f:
                    json.dump(dbCfg_json, f, indent=4)

            # siteCfg.json
            siteCfg_json = {"insitution": self.institution["institution"]}
            oid_fs_siteCfg = self._put_json_as_file(siteCfg_json, "siteCfg.json")
            config_siteCfg = {"filename": "siteCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "siteCfg", "format": "fs.files", "data_id": oid_fs_siteCfg}
            oid_siteCfg = self._write_index(self.DB_localdb.config, config_siteCfg)
            if self.debug:
                with open("debug_siteCfg.json", "w") as f:
                    json.dump(siteCfg_json, f, indent=4)

            # userCfg.json
            userCfg_json = {"description": "default", "institution": self.institution["code"], "userName": self.user["userName"]}
            oid_fs_userCfg = self._put_json_as_file(userCfg_json, "userCfg.json")
            config_userCfg = {"filename": "userCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "userCfg", "format": "fs.files", "data_id": oid_fs_userCfg}
            oid_userCfg = self._write_index(self.DB_localdb.config, config_userCfg)
            if self.debug:
                with open("debug_userCfg.json", "w") as f:
                    json.dump(userCfg_json, f, indent=4)

            # chipCfg.json (beforeCfg)
            chipCfg_json = convert_config_bdaq_to_yarr(chip_config_in)
            oid_fs_chipCfg = self._put_json_as_file(chipCfg_json, "chipCfg.json")
            config_chipCfg = {"filename": "chipCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "beforeCfg", "format": "fs.files", "data_id": oid_fs_chipCfg}
            oid_chipCfg_before = self._write_index(self.DB_localdb.config, config_chipCfg)
            if self.debug:
                with open("debug_chipCfg.json", "w") as f:
                    json.dump(chipCfg_json, f, indent=4)

            # chipCfg.json (afterCfg)
            # This file is not uploaded (at least for an analog scan). The index in testRun links to the chipCfg.json.before
            config_chipCfg = {"filename": "chipCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "afterCfg", "format": "fs.files", "data_id": oid_fs_chipCfg}
            oid_chipCfg_after = self._write_index(self.DB_localdb.config, config_chipCfg)

            # scanCfg
            scanCfg_json = {"scan": {"histogrammer": {"0": {"algorithm": "OccupancyMap", "config": {}}, "1": {"algorithm": "TotMap", "config": {}}, "2": {"algorithm": "Tot2Map", "config": {}}, "3": {"algorithm": "L1Dist", "config": {}}, "4": {"algorithm": "HitsPerEvent", "config": {}}, "n_count": 5}, "loops": [{"config": {"max": 64, "min": 0, "step": 1}, "loopAction": "Rd53aMaskLoop"}, {"config": {"max": 50, "min": 0, "step": 1, "nSteps": 5}, "loopAction": "Rd53aCoreColLoop"}, {"config": {"count": 100, "delay": 48, "extTrigger": False, "frequency": 30000, "noInject": False, "time": 0}, "loopAction": "Rd53aTriggerLoop"}, {"loopAction": "StdDataLoop"}], "name": scan_id, "prescan": {"InjEnDig": 0, "LatencyConfig": 48, "GlobalPulseRt": chip_config_in["registers"]["GLOBAL_PULSE_ROUTE"]}}}
            if scan_id in ["analog_scan"]:
                scanCfg_json["scan"].update({"analysis": {"0": {"algorithm": "OccupancyAnalysis", "config": {"createMask": True}}, "1": {"algorithm": "L1Analysis"}, "n_count": 2}})
                scanCfg_json["scan"].update({"prescan": {"InjVcalDiff": 0, "InjVcalHigh": scan_config["scan_config"]["VCAL_HIGH"], "InjVcalMed": scan_config["scan_config"]["VCAL_MED"]}})
            if scan_id in ["threshold_scan"]:
                scanCfg_json["scan"].update({"analysis": {"0": {"algorithm": "ScurveFitter"}, "1": {"algorithm": "OccupancyAnalysis"}, "n_count": 2}})
                scanCfg_json["scan"].update({"loops": {"config": {"max": scan_config["scan_config"]["VCAL_HIGH_stop"] - scan_config["scan_config"]["VCAL_MED"], "min": scan_config["scan_config"]["VCAL_HIGH_start"] - scan_config["scan_config"]["VCAL_MED"], "step": scan_config["scan_config"]["VCAL_HIGH_step"], "parameter": "InjVcalDiff"}, "loopAction": "Rd53aParameterLoop"}})
                scanCfg_json["scan"].update({"prescan": {"InjVcalDiff": 0, "InjVcalHigh": 1000, "InjVcalMed": 1000}})
            oid_fs_scanCfg = self._put_json_as_file(scanCfg_json, "scanCfg.json")
            config_scanCfg = {"filename": "scanCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "scanCfg", "format": "fs.files", "data_id": oid_fs_scanCfg}
            oid_scanCfg = self._write_index(self.DB_localdb.config, config_scanCfg)
            if self.debug:
                with open("debug_scanCfg.json", "w") as f:
                    json.dump(scanCfg_json, f, indent=4)

            # Prepare run data -> localdb.testRun
            if testRun is not None:
                oid_testRun = testRun
            else:
                run_data = {"exec": run_name, "runNumber": 1, "stopwatch": {"analysis": 0, "config": 0, "processing": 0, "scan": 0}, "testType": scan_config["run_config"]["scan_id"], "timestamp": timestamp, "targetCharge": -1, "targetTot": -1, "QC": False, "stage": "Testing", "chipType": scan_config["run_config"]["chip_type"].upper(), "address": str(self.institution["_id"]), "user_id": str(self.user["_id"]), "environment": False if dcs_data is None else True, "passed": True, "qcTest": False, "qaTest": False, "summary": False, "startTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "finishTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "ctrlCfg": oid_ctrlCfg, "dbCfg": oid_dbCfg, "siteCfg": oid_siteCfg, "userCfg": oid_userCfg, "scanCfg": oid_scanCfg, "plots": []}

                if scan_id in ["digital_scan", "analog_scan"]:
                    run_data["plots"].extend(["L1Dist", "OccupancyMap", "EnMask"])
                if scan_id in ["threshold_scan"]:
                    run_data["plots"].extend(["ThresholdDist-0", "ThresholdMap-0", "NoiseDist-0", "NoiseMap-0", "Chi2Dist-0", "Chi2Map-0", "sCurve-0"])
                if scan_id in ["global_threshold_tuning", "local_threshold_tuning"]:
                    run_data["targetCharge"] = int(_convert_to_e(scan_config["scan_config"]["VCAL_HIGH"] - scan_config["scan_config"]["VCAL_MED"])[0], 0)

                self.log.debug("testRun data:\n{}".format(run_data))
                oid_testRun = self._write_index(self.DB_localdb.testRun, run_data)

            # Prepare component test run data -> localdb.componentTestRun
            componentRun_data = {"config": chip_config["settings"]["chip_config_file"], "enable": 1, "locked": 0, "rx": chip_config["settings"]["receiver"], "tx": 0, "name": chip_component["name"], "geomId": chip_component["chip_id"], "component": ch_oid_component, "chip": ch_oid, "testRun": oid_testRun, "environment": "..." if dcs_data is None else oid_environment_data, "beforeCfg": oid_chipCfg_before, "afterCfg": oid_chipCfg_after}
            if scan_id in ["digital_scan", "analog_scan", "threshold_scan"]:
                componentRun_data.update({"attachments": [{"code": oid_fs_EnMask, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "EnMask", "description": "describe", "contentType": "json", "filename": "EnMask.json"}, {"code": oid_fs_OccupancyMap, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "OccupancyMap", "description": "describe", "contentType": "json", "filename": "OccupancyMap.json"}, {"code": oid_fs_L1Dist, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "L1Dist", "description": "describe", "contentType": "json", "filename": "L1Dist.json"}]})
            if scan_id in ["threshold_scan"]:
                componentRun_data["attachments"].extend(
                    [
                        {"code": oid_fs_ThresholdDist_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "ThresholdDist-0", "description": "describe", "contentType": "dat", "filename": "ThresholdDist-0.dat"},
                        {"code": oid_fs_ThresholdMap_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "ThresholdMap-0", "description": "describe", "contentType": "dat", "filename": "ThresholdMap-0.dat"},
                        {"code": oid_fs_NoiseDist_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "NoiseDist-0", "description": "describe", "contentType": "dat", "filename": "NoiseDist-0.dat"},
                        {"code": oid_fs_NoiseMap_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "NoiseMap-0", "description": "describe", "contentType": "dat", "filename": "NoiseMap-0.dat"},
                        {"code": oid_fs_Chi2Dist_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "Chi2Dist-0", "description": "describe", "contentType": "dat", "filename": "Chi2Dist-0.dat"},
                        {"code": oid_fs_Chi2Map_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "Chi2Map-0", "description": "describe", "contentType": "dat", "filename": "Chi2Map-0.dat"},
                        {"code": oid_fs_sCurve_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "sCurve-0", "description": "describe", "contentType": "json", "filename": "sCurve-0.json"},
                    ]
                )
            self.log.debug("componentTestRun data:\n{}".format(componentRun_data))
            oid_componentTestRun = self._write_index(self.DB_localdb.componentTestRun, componentRun_data)

            self.log.success("Successfully uploaded scan {}".format(run_name))

            return oid_componentTestRun, oid_testRun

        # Prepare component data -> localdb.component
        is_module_data = False
        with tb.open_file(in_file, "r") as f:
            try:  # module file
                node = f.root.configuration_in.module
                is_module_data = True
                self.log.debug("Found module data.")
                components = self._read_components(node=node, is_module_data=is_module_data)
            except tb.NoSuchNodeError:  # single chip file
                node = f.root.configuration_in.chip
                components = self._read_components(node=node, is_module_data=is_module_data)

        # Upload components (module and chips)
        # Module
        if 'module' in components:
            mo_component_data = {"serialNumber": components["module"]['name'], "chipId": -1, "name": components["module"]['name'], "chipType": components["module"]['chip_type'], "componentType": "module", "childen": len(components["module"]['chips']), "proDB": False, "address": str(self.institution["_id"]), "user_id": str(self.user["_id"])}
            oid_mo_component = self._check_module_component(components)
            if oid_mo_component is None:
                oid_mo_component = self._write_index(self.DB_localdb.component, mo_component_data)

        # Chips
        ch_oids = []  # chip oid in chip collection
        ch_oid_components = []  # chip oid in component collection
        for i, ch in enumerate(components["module"]['chips']):
            # Chip collection
            chip_data = {"name": components["module"]['chips'][ch]["name"], "chipId": components["module"]['chips'][ch]["chip_id"], "chipType": components["module"]['chips'][ch]["chip_type"], "componentType": "front-end_chip"}
            oid_chip = self._check_chip(components["module"]['chips'][ch])
            if oid_chip is None:
                oid_chip = self._write_index(self.DB_localdb.chip, chip_data)
            ch_oids.append(oid_chip)

            # Component collection
            ch_component_data = {"serialNumber": components["module"]['chips'][ch]["name"], "chipId": components["module"]['chips'][ch]["chip_id"], "name": components["module"]['chips'][ch]["name"], "chipType": components["module"]['chips'][ch]['chip_type'], "componentType": "front-end_chip", "childen": -1, "proDB": False, "address": str(self.institution["_id"]), "user_id": str(self.user["_id"])}
            ch_oid_component = self._check_chip_component(components['module']['chips'][ch])
            if ch_oid_component is None:
                ch_oid_component = self._write_index(self.DB_localdb.component, ch_component_data)
            ch_oid_components.append(ch_oid_component)

            # Child-Parent relation
            if 'module' in components:
                child_parent_data = {"parent": oid_mo_component, "child": ch_oid_component, "chipId": components["module"]['chips'][ch]["chip_id"], "geomId": -1, "status": 'active'}
                oid_childParentRelation = self._check_child_parent(oid_mo_component, ch_oid_component)
                if oid_childParentRelation is None:
                    oid_childParentRelation = self._write_index(self.DB_localdb.childParentRelation, child_parent_data)

        with tb.open_file(in_file, "r") as f:
            # Prepare environmental data -> localdb.environment
            # if environment_data is not None:
            #     oid_environment_data = environment_data
            # else:
            try:
                dcs_data = f.root.environmental_data[:]
            except tb.NoSuchNodeError:
                dcs_data = None
            if dcs_data is not None:
                oid_environment_data = self._register_DCS_data(dcs_data)
            else:
                oid_environment_data = '...'

            if is_module_data:
                chip_sns = []
                chip_sn_to_chip_id = {}
                for chip_node in f.root.configuration_in.module.chips:
                    chip_settings = fill_dict_from_conf_table(chip_node.settings)
                    chip_sns.append(chip_settings['chip_sn'])
                    chip_sn_to_chip_id.update({chip_settings['chip_sn']: chip_settings['chip_id']})
            else:
                chip_settings = fill_dict_from_conf_table(f.root.configuration_in.chip.settings)
                chip_sns = [chip_settings['chip_sn']]
                chip_sn_to_chip_id = {chip_settings['chip_sn']: chip_settings['chip_id']}
            self.log.debug("Chip S/N: {0}, chip_sn_to_chip_id: {1}".format(chip_sns, chip_sn_to_chip_id))

        for i, k in enumerate(components['module']['chips']):
            chip_component = components['module']['chips'][k]
            _upload_scan_data(is_module_data, chip_component, ch_oid_components[i], ch_oids[i])
